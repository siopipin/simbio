import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';
import 'package:simbio/src/app.dart';
import 'package:simbio/src/auth/bloc/authentication/bloc.dart';
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/util/bloc_delegate.dart';

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  final authRepository = AuthRepository();
  runApp(
    BlocProvider<AuthenticationBloc>(
      builder: (context) {
        return AuthenticationBloc(authRepository: authRepository)..dispatch(AppStarted());
      },
      child: App(authRepository: authRepository),
    )
  );
}