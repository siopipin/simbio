class AuthModel {
  String email;
  int id;
  String token;
  String message;

  AuthModel({this.email, this.id, this.token, this.message});

  AuthModel.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    id = json['id'];
    token = json['token'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['id'] = this.id;
    data['token'] = this.token;
    data['message'] = this.message;
    return data;
  }
}