import 'dart:async';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:simbio/src/auth/bloc/authentication/bloc.dart';
import 'package:simbio/src/auth/bloc/login/bloc.dart';
import 'package:simbio/src/auth/models/user_model.dart';
import 'package:simbio/src/auth/repository/auth_repository.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthRepository userRepository;
  final AuthenticationBloc authenticationBloc;

  LoginBloc({
    @required this.userRepository,
    @required this.authenticationBloc,
  })  : assert(userRepository != null),
        assert(authenticationBloc != null);

  @override
  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield LoginLoading();
      try {
        final UserModel data = await userRepository.postLogin(
          email: event.email,
          password: event.password,
        );

        authenticationBloc.dispatch(LoggedIn(data: data));
        yield LoginInitial();
      } catch (error) {
        yield LoginFailure(error: error.toString());
      }
    }
  }
}