import 'dart:async';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:simbio/src/auth/models/user_model.dart';
import 'package:simbio/src/auth/repository/auth_repository.dart';

import 'bloc.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final AuthRepository authRepository;

  AuthenticationBloc({@required this.authRepository})
      : assert(authRepository != null);

  @override
  AuthenticationState get initialState => AuthenticationUninitialized();

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      final String hasToken = await authRepository.hasToken();
      if (hasToken != null) {
        yield AuthenticationLoading();
        try {
          final UserModel data =
              await authRepository.decodeToken(token: hasToken);
          yield AuthenticationAuthenticated(userModel: data);
        } catch (e) {
          yield AuthenticationUnauthenticated();
        }
      } else {
        yield AuthenticationUnauthenticated();
      }
    }

    if (event is LoggedIn) {
      yield AuthenticationLoading();
      await authRepository.persistToken(event.data);
      try {
        final UserModel data =
            await authRepository.decodeToken(token: event.data.token);
        yield AuthenticationAuthenticated(userModel: data);
      } catch (e) {
        yield AuthenticationUnauthenticated();
      }
      yield AuthenticationAuthenticated();
    }

    if (event is LoggedOut) {
      yield AuthenticationLoading();
      await authRepository.deleteToken();
      yield AuthenticationUnauthenticated();
    }
  }
}
