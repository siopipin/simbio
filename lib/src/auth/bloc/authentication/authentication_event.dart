import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:simbio/src/auth/models/user_model.dart';

abstract class AuthenticationEvent extends Equatable {
  AuthenticationEvent([List props = const []]) : super(props);
}

class AppStarted extends AuthenticationEvent {
  @override
  String toString() => 'AppStarted';
}

class LoggedIn extends AuthenticationEvent {
  final UserModel data;

  LoggedIn({@required this.data}) : super([data]);

  @override
  String toString() => 'AppStarted.LoggedIn { log: ${data.id} }';
}

class LoggedOut extends AuthenticationEvent {
  @override
  String toString() => 'AuthenticationEvent.LoggedOut';
}