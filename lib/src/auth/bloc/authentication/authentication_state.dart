import 'package:equatable/equatable.dart';
import 'package:simbio/src/auth/models/user_model.dart';

abstract class AuthenticationState extends Equatable {
  UserModel authModel;
}

class AuthenticationUninitialized extends AuthenticationState {
  @override
  String toString() => 'AuthenticationUninitialized';
}

class AuthenticationAuthenticated extends AuthenticationState {
  UserModel userModel;
  AuthenticationAuthenticated({userModel}) {
    this.userModel = userModel;
  }
  @override
  String toString() => 'AuthenticationAuthenticated';
}

class AuthenticationUnauthenticated extends AuthenticationState {
  @override
  String toString() => 'AuthenticationUnauthenticated';
}

class AuthenticationLoading extends AuthenticationState {
  @override
  String toString() => 'AuthenticationLoading';
}