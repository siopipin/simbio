import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simbio/src/auth/bloc/login/bloc.dart';
import 'package:simbio/src/util/pustaka.dart';

class LoginForm extends StatefulWidget {
  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final emailController = TextEditingController(text: "tekniker1@simbio.com");
  final passwordController = TextEditingController(text: "123");

  @override
  Widget build(BuildContext context) {
    final loginBloc = BlocProvider.of<LoginBloc>(context);

    _onLoginButtonPressed() {
      loginBloc.dispatch(LoginButtonPressed(
        email: emailController.text,
        password: passwordController.text,
      ));
    }

    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is LoginFailure) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text('${state.error}'),
              backgroundColor: Colors.red,
            ),
          );
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {

          //Login
          final loginButton = Padding(
            padding: EdgeInsets.symmetric(vertical: 16.0),
            child: Material(
              borderRadius: BorderRadius.circular(30.0),
              shadowColor: COLOR.primary(),
              elevation: 5.0,
              child: MaterialButton(
                minWidth: 200.0,
                height: 42.0,
                onPressed:
                    state is! LoginLoading ? _onLoginButtonPressed : null,
                color: COLOR.secondary(),
                child: Text('Log In', style: TextStyle(color: Colors.white)),
              ),
            ),
          );

          return Form(
            child: Column(
              children: [
                emailField(),
                SizedBox(height: 8.0),
                passwordField(),
                loginButton,
                Container(
                  child: state is LoginLoading
                      ? CircularProgressIndicator()
                      : null,
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget emailField() {
    return Theme(
      data: ThemeData(
          hintColor: Colors.white,
          primaryColor: COLOR.secondary(),
          primaryColorDark: COLOR.secondary()),
      child: TextFormField(
        style: TextStyle(color: Colors.white),
        autofocus: false,
        keyboardType: TextInputType.emailAddress,
        decoration: new InputDecoration(
            hintStyle: TextStyle(color: Colors.white),
            prefixIcon: const Icon(Icons.email),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 0.0),
            ),
            hintText: 'Email',
            contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white, width: 0.0),
                borderRadius: BorderRadius.circular(10.0))),
        controller: emailController,
      ),
    );
  }

  Widget passwordField() {
    return Theme(
      data: ThemeData(
          hintColor: Colors.white,
          primaryColor: COLOR.secondary(),
          primaryColorDark: COLOR.secondary()),
      child: TextFormField(
        obscureText: true,
        style: TextStyle(color: Colors.white),
        autofocus: false,
        keyboardType: TextInputType.text,
        decoration: new InputDecoration(
            hintStyle: TextStyle(color: Colors.white),
            prefixIcon: const Icon(Icons.vpn_key),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 0.0),
            ),
            hintText: 'Password',
            contentPadding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white, width: 0.0),
                borderRadius: BorderRadius.circular(10.0))),
        controller: passwordController,
      ),
    );
  }
}
