import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simbio/src/auth/bloc/authentication/bloc.dart';
import 'package:simbio/src/auth/bloc/login/bloc.dart';
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/auth/screens/auth_form.dart';
import 'package:simbio/src/util/pustaka.dart';

class AuthScreen extends StatelessWidget {
  AuthScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authRepository = AuthRepository();
    return Scaffold(
        body: BlocProvider(
      builder: (context) {
        return LoginBloc(
          authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
          userRepository: authRepository,
        );
      },
      child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: new BoxDecoration(
            gradient: new LinearGradient(
                colors: [COLOR.linearone(), COLOR.lineartwo()],
                begin: const FractionalOffset(0.0, 0.0),
                end: const FractionalOffset(1.0, 1.0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp),
          ),
          child: Center(
              child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 20, bottom: 40),
                    child: new Image(
                        width: SIZE.screen(context).width / 2 -20,
                        height: SIZE.screen(context).height / 5 + 30,
                        fit: BoxFit.fill,
                        image: new AssetImage('assets/images/logo.png')),
                  ),
                  Center(
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: LoginForm(),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: new Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Text(
                                "NB: Silahkan login",
                                style: TextStyle(color: Colors.white),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  )
                ],
              )
            ],
          ))),
    ));
  }
}
