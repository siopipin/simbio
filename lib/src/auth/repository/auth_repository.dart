import 'dart:async';
import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' show Client;
import 'package:simbio/src/auth/models/user_model.dart';
import 'package:simbio/src/util/pustaka.dart';

import 'package:jaguar_jwt/jaguar_jwt.dart';

class AuthRepository {
  Client client = Client();
  final storage = new FlutterSecureStorage();

  Future<UserModel> postLogin(
      {@required String email, @required String password}) async {
    final body = {
      'email': email,
      'password': password,
    };
    final String url = '${URL.api()}auth/login';
    try {
      final response = await client.post(url, body: body);
      if (response.statusCode == 200) {
        final result = UserModel.fromJson(json.decode(response.body));
        return result;
      } else if (response.statusCode == 401) {
        throw Exception(json.decode(response.body)['message'].toString());
      } else {
        throw Exception(
            'Request Error with Status Code: ${response.statusCode}');
      }
    } on Exception {
      rethrow;
    }
  }

  Future decodeToken({@required String token}) async {
    final String data = token;
    final parts = data.split('.');
    final payload = parts[1];
    final String decoded = B64urlEncRfc7515.decodeUtf8(payload);
    print(decoded);
    final result = UserModel.fromJson(json.decode(decoded));
    await storage.write(key: 'nama', value: result.nama);
    return result;
  }

  Future<void> deleteToken() async {
    await storage.delete(key: 'token');
    return;
  }

  Future<void> persistToken(UserModel token) async {
    await storage.write(key: 'token', value: token.token);
    await storage.write(key: 'id', value: token.id.toString());
    await storage.write(key: 'email', value: token.email);
    await storage.write(key: 'nama', value: token.nama);
    await storage.write(key: 'privilege', value: token.privilege.toString());
    return;
  }

  Future<String> hasToken() async {
    String token = await storage.read(key: 'token');
    if (token != null) {
      return token;
    } else {
      return null;
    }
  }

  Future<String> hasId() async {
    String data = await storage.read(key: 'id');
    if (data != null) {
      return data;
    } else {
      return null;
    }
  }

  Future<String> hasPrivilege() async {
    String data = await storage.read(key: 'privilege');
    if (data != null) {
      return data;
    } else {
      return null;
    }
  }

  Future<String> hasEmail() async {
    String data = await storage.read(key: 'email');
    if (data != null) {
      return data;
    } else {
      return null;
    }
  }

  Future<String> hasName() async {
    String data = await storage.read(key: 'nama');
    if (data != null) {
      return data;
    } else {
      return null;
    }
  }

  Future<String> currentStep() async {
    String data = await storage.read(key: 'currentstep');
    if (data != null) {
      return data;
    } else {
      return null;
    }
  }
}
