class UserModel {
  String nama;
  String email;
  int id;
  int privilege;
  String token;

  UserModel({this.nama, this.email, this.id, this.privilege, this.token});

  UserModel.fromJson(Map<String, dynamic> json) {
    nama = json['nama'];
    email = json['email'];
    id = json['id'];
    privilege = json['privilege'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nama'] = this.nama;
    data['email'] = this.email;
    data['id'] = this.id;
    data['privilege'] = this.privilege;
    data['token'] = this.token;
    return data;
  }
}
