import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:meta/meta.dart';
import 'package:http/http.dart' show Client;
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/util/response_model.dart';

class HomeProvider {
  Client client = Client();

  Future<ResponseModel> updateFCMToken(
      {@required int id, @required String tokenFCM}) async {
    final token = await AuthRepository().hasToken();
    final url = '${URL.api()}tekniker/updatefcm/';

    final body = json.encode({"id": id, "token": tokenFCM});
    print('BODY YANG DI KIRIM : $body');
    final res = await client.put(url,
        headers: {
          HttpHeaders.authorizationHeader: 'Barer $token',
          'Content-type': 'application/json',
          'Accept': 'application/json'
        },
        body: body);
    print(res.statusCode);
    print(res.body);
    final data = json.decode(res.body);
    data['statuscode'] = res.statusCode;
    if (res.statusCode == 200) {
      data['message'] = "Successfully";
      final result = ResponseModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 400) {
      data['message'] = "Cannot get data from server";
      final result = ResponseModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 401) {
      data['message'] = "Not authorized or invalid token";
      final result = ResponseModel.fromJson(data);
      return result;
    } else {
      data['message'] = "Error from server";
      final result = ResponseModel.fromJson(data);
      return result;
    }
  }
}
