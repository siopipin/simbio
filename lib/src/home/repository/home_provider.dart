import 'dart:async';
import 'package:meta/meta.dart';
import 'package:simbio/src/home/repository/home_repository.dart';
import 'package:simbio/src/util/response_model.dart';

class HomeRepository {
  final HomeProvider homeProvider = HomeProvider();

  Future<ResponseModel> updateFCMToken(
          {@required int id, @required String token}) =>
      homeProvider.updateFCMToken(id: id, tokenFCM: token);
}
