import 'package:rxdart/rxdart.dart';
import 'package:simbio/src/home/repository/home_provider.dart';
import 'package:meta/meta.dart';
import 'package:simbio/src/util/response_model.dart';

class HomeBLOC {
  final HomeRepository homeRepository = HomeRepository();

  //Event
  doUpdateTokenFcm({@required int id, @required String token}) async {
    ResponseModel responseModel =
        await homeRepository.updateFCMToken(token: token, id: id);
    return responseModel;
  }
}

final homeBLOC = HomeBLOC();
