import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:simbio/src/absensi/blocs/absensi_bloc.dart';
import 'package:simbio/src/absensi/screens/absensi_screen.dart';
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/home/bloc/home_bloc.dart';
import 'package:simbio/src/landing/model/message_model.dart';
import 'package:simbio/src/order/models/orders_model.dart';
import 'package:simbio/src/order/screens/order_riwayat.dart';
import 'package:simbio/src/order/screens/order_total.dart';
import 'package:simbio/src/util/formatter_util.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/util/response_model.dart';
import 'package:simbio/src/widgets/curver_clipper_widget.dart';

//Scan
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import 'package:simbio/src/order/screens/detail_order_scan.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:simbio/src/widgets/custom_dialog_ui.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final List<Message> messages = [];
  String globalFCMToken;
  String idclient;
  final storage = new FlutterSecureStorage();

  OrdersModel dataorder;
  var datatotal;
  String totalorder;
  String totalselesai;

  @override
  void initState() {
    super.initState();
    initIdClient();
    readTokenFCM();
    Stream<String> fcmStream = _firebaseMessaging.onTokenRefresh;
    fcmStream.listen((token) {
      print('FCM DEFAULT TOKEN:  $token');
      saveToken(token);
    });
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        final notification = message['notification'];
        setState(() {
          messages.add(Message(
              title: notification['title'], body: notification['body']));
        });
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: ListTile(
              title: Text(message['notification']['title']),
              subtitle: Text(message['notification']['body']),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        );
      },
      onLaunch: (Map<String, dynamic> message) async {
        final notification = message['data'];
        setState(() {
          messages.add(Message(
            title: '${notification['title']}',
            body: '${notification['body']}',
          ));
        });
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: ListTile(
              title: Text(message['notification']['title']),
              subtitle: Text(message['notification']['body']),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        );
      },
      onResume: (Map<String, dynamic> message) async {
        final notification = message['data'];
        setState(() {
          messages.add(Message(
            title: '${notification['title']}',
            body: '${notification['body']}',
          ));
        });
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: ListTile(
              title: Text(message['notification']['title']),
              subtitle: Text(message['notification']['body']),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        );
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
  }

  void iOSPermission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {});
  }

  void initIdClient() async {
    String temp = await AuthRepository().hasId();
    setState(() {
      idclient = temp;
    });
  }

  saveToken(token) async {
    if (globalFCMToken == token) {
      print("TOKEN SAMA DI STORAGE");
      globalFCMToken = token;
    } else if (globalFCMToken == null) {
      globalFCMToken = token;
      await storage.write(key: 'fcm', value: token);
      print("SEDANG MENULIS KE GLOBALFCM TOKEN");
      print("TOKEN yang AKAN DI KIRIM : $token");
      await homeBLOC.doUpdateTokenFcm(id: int.parse(idclient), token: token);
      print("TELAH KIRIM UPDATE TOKEN");
    } else if (globalFCMToken != token) {
      print("TOKEN TIDAK SAMA");
      await storage.write(key: 'fcm', value: token);
      await homeBLOC.doUpdateTokenFcm(id: int.parse(idclient), token: token);
    }
  }

  readTokenFCM() async {
    String res = await storage.read(key: 'fcm');
    print('TOKEN DI STORAGE : $res');
    if (res == null || res == 'null' || res.isEmpty) {
      globalFCMToken = null;
    } else if (res != null) {
      globalFCMToken = res;
    }
  }

  String result;
  String id;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: SIZE.screen(context).height,
      width: SIZE.screen(context).width,
      child: Stack(
        children: <Widget>[
          ClipPath(
              clipper: CurveClipper(),
              child: ChildCurveContainer.container(context)),
          pemberitahuan(),
          rincianPekerja()
        ],
      ),
    );
  }

  Widget pemberitahuan() {
    return Positioned(
      width: SIZE.screen(context).width,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: Container(
          height: 80,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Icon(
                  Icons.message,
                  size: 50,
                  color: COLOR.primary(),
                ),
              ),
              Expanded(
                  flex: 6,
                  child: messages.length == 0
                      ? ListTile(
                          title: Text(
                            "Pemberitahuan",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 17),
                          ),
                          subtitle: Text(
                              "ID Order A001 Deadline 20 September 2019 !"),
                        )
                      : ListView(
                          children: messages.map(buildMessage).toList(),
                        ))
            ],
          ),
        ),
      ),
    );
  }

  Widget rincianPekerja() {
    return Positioned(
      top: 100,
      width: SIZE.screen(context).width,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 10,
        ),
        child: Container(
          height: 200,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Color(0xffeff2f3),
                offset: Offset(1, 5.0),
                blurRadius: 3.0,
              ),
            ],
          ),
          child: Column(
            children: <Widget>[
              detailOrderMenu(),
              Divider(
                color: Colors.grey,
                height: 1,
              ),
              scannerMenu(),
            ],
          ),
        ),
      ),
    );
  }

  Widget detailOrderMenu() {
    return Expanded(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: GestureDetector(
                  onTap: () => Navigator.push(context,
                      MaterialPageRoute(builder: (context) => OrderTotalUI())),
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: 70,
                        child: new Icon(
                          Icons.open_in_browser,
                          size: 50,
                          color: COLOR.primary(),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Semua Order",
                              style: TextStyle(
                                color: COLOR.primary(),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
          ),
          VerticalDivider(
            color: Colors.grey,
            width: 1,
          ),
          Expanded(
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: GestureDetector(
                  onTap: () {
                    return Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => OrderRiwayatUI()));
                  },
                  child: Row(
                    children: <Widget>[
                      Container(
                        width: 75,
                        child: new Icon(
                          Icons.insert_chart,
                          size: 50,
                          color: COLOR.primary(),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Riwayat Order",
                              style: TextStyle(
                                color: COLOR.primary(),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
          ),
        ],
      ),
    );
  }

  Widget scannerMenu() {
    return Expanded(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("SCAN PRODUK",
                      style: TextStyle(
                        color: COLOR.primary(),
                        fontWeight: FontWeight.bold,
                      )),
                  GestureDetector(
                    onTap: _scanQR,
                    child: Container(
                      width: 70,
                      child: new Icon(
                        Icons.photo_camera,
                        size: 50,
                        color: COLOR.primary(),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          VerticalDivider(
            color: Colors.grey,
            width: 1,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("SCAN ABSENSI",
                      style: TextStyle(
                        color: COLOR.primary(),
                        fontWeight: FontWeight.bold,
                      )),
                  GestureDetector(
                    onTap: _scanAbsensi,
                    child: Container(
                      width: 70,
                      child: new Icon(
                        Icons.check_box,
                        size: 50,
                        color: COLOR.primary(),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future _scanQR() async {
    try {
      String qrResult = await BarcodeScanner.scan();

      const start = ".id/";
      const end = "/20";

      final startIndex = qrResult.indexOf(start);
      final endIndex = qrResult.indexOf(end, startIndex + start.length);

      String id = qrResult.substring(startIndex + start.length, endIndex);
      print(id);
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => DetailOrderScanScreen(
                    id: id,
                  )));
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          result = "Camera permission was denied";
        });
      } else {
        setState(() {
          result = "Unknown Error $ex";
        });
      }
    } on FormatException {
      setState(() {
        result = "You pressed the back button before scanning anything";
      });
    } catch (ex) {
      setState(() {
        result = "Unknown Error $ex";
      });
    }
  }

  Future _scanAbsensi() async {
    try {
      String qrResult = await BarcodeScanner.scan();
      const start = "absensi/";

      final startIndex = qrResult.indexOf(start);

      String tanggal =
          qrResult.substring(startIndex + start.length, qrResult.length);

      print("home_screen.dart / scanAbsensi / tanggal dari scan : $tanggal");

      Future.delayed(Duration.zero, () async {
        DateTime formattedDate = DateTime.now();
        String now = formatDateTime.format(formattedDate);
        String idtekniker = await AuthRepository().hasId();
        String nama = await AuthRepository().hasName();
        print("home_screen.dart / scanAbsensi / testing : I AM HERE");
        ResponseModel responseModel = await absensiBLOC.doUpdateAbsensi(
            idTekniker: idtekniker,
            tanggal: formatDate.format(DateTime.parse(tanggal)),
            tglkeluar: now);

        if (responseModel.statuscode == 200) {
          showDialog(
            context: context,
            builder: (BuildContext context) => CustomDialog(
              title: "Absensi berhasil",
              description: "Absensi $tanggal",
              buttonText: "Lanjut",
            ),
          );
        } else {
          ResponseModel responseModel = await absensiBLOC.doPostAbsensi(
              idTekniker: idtekniker,
              tanggal: formatDate.format(DateTime.parse(tanggal)),
              tglmasuk: now);

          if (responseModel.statuscode == 200) {
            showDialog(
              context: context,
              builder: (BuildContext context) => CustomDialog(
                title: "Selamat Pagi, $nama",
                description: "Absensi $tanggal",
                buttonText: "Lanjut",
              ),
            );
          }
        }
      });
    } on PlatformException catch (ex) {
      if (ex.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          result = "Camera permission was denied";
        });
      } else {
        setState(() {
          result = "Unknown Error $ex";
        });
      }
    } on FormatException {
      setState(() {
        result = "You pressed the back button before scanning anything";
      });
    } catch (ex) {
      setState(() {
        result = "Unknown Error $ex";
      });
    }
  }

  Widget buildMessage(Message message) => ListTile(
        title: Text(
          message.title,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
        ),
        subtitle: Text(message.body),
      );
}
