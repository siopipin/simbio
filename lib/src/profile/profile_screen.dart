import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simbio/src/absensi/screens/absensi_screen.dart';
import 'package:simbio/src/auth/bloc/authentication/bloc.dart';
import 'package:simbio/src/util/firebase_notification_handler.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/widgets/curver_clipper_widget.dart';

class ProfileScreen extends StatefulWidget {
  ProfileScreen({Key key}) : super(key: key);

  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  void initState() { 
    super.initState();
    new FirebaseNotifications().setUpFirebase(context);
  }
  @override
  Widget build(BuildContext context) {
    final AuthenticationBloc authenticationBloc =
        BlocProvider.of<AuthenticationBloc>(context);
    return Container(
      height: SIZE.screen(context).height,
      width: SIZE.screen(context).width,
      child: Stack(
        children: <Widget>[
          ClipPath(
              clipper: CurveClipper(),
              child: ChildCurveContainer.container(context)),
          Positioned(
            width: SIZE.screen(context).width,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Container(
                padding: EdgeInsets.only(bottom: 156),
                height: SIZE.screen(context).height,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                          width: SIZE.screen(context).width,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xffeff2f3),
                                    offset: Offset(1, 5.0),
                                    blurRadius: 3.0)
                              ]),
                          child: Container(
                            padding: EdgeInsets.all(20),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Profile",
                                      style: TextStyle(
                                          color: COLOR.ketiga(), fontSize: 28),
                                    )
                                  ],
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 10),
                                  width: SIZE.screen(context).width,
                                  child: BlocBuilder<AuthenticationBloc,
                                      AuthenticationState>(
                                    builder: (context, state) {
                                      if (state
                                          is AuthenticationAuthenticated) {
                                        return Row(
                                          children: <Widget>[
                                            Container(
                                              margin:
                                                  EdgeInsets.only(right: 30),
                                              child: Icon(
                                                Icons.photo,
                                                size: 50,
                                              ),
                                            ),
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  state.userModel.nama,
                                                  style: TextStyle(
                                                      fontSize: 17,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                SizedBox(
                                                  height: 5,
                                                ),
                                                Text(state.userModel.email)
                                              ],
                                            )
                                          ],
                                        );
                                      } else {
                                        return Center(
                                          child: CircularProgressIndicator(),
                                        );
                                      }
                                    },
                                  ),
                                )
                              ],
                            ),
                          )),
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        padding: EdgeInsets.only(top: 20),
                        child: ListView(
                          children: <Widget>[
                            Container(
                              height: SIZE.screen(context).height,
                              child: Column(
                                children: <Widget>[
                                  // GestureDetector(
                                  //   onTap: () => Navigator.push(
                                  //       context,
                                  //       MaterialPageRoute(
                                  //           builder: (context) =>
                                  //               AbsensiScreen())),
                                  //   child: ListTile(
                                  //     leading: Icon(Icons.calendar_today),
                                  //     title: Text('History Absensi'),
                                  //     trailing: Icon(Icons.arrow_forward),
                                  //   ),
                                  // ),
                                  // GestureDetector(
                                  //   onTap: () {},
                                  //   child: ListTile(
                                  //     leading: Icon(Icons.attach_money),
                                  //     title: Text('History Intensif'),
                                  //     trailing: Icon(Icons.arrow_forward),
                                  //   ),
                                  // ),
                                  // GestureDetector(
                                  //   onTap: () {},
                                  //   child: ListTile(
                                  //     leading: Icon(Icons.settings),
                                  //     title: Text('Pengaturan'),
                                  //     trailing: Icon(Icons.arrow_forward),
                                  //   ),
                                  // ),
                                  GestureDetector(
                                    onTap: () {
                                      authenticationBloc.dispatch(LoggedOut());
                                    },
                                    child: ListTile(
                                      leading: Icon(Icons.exit_to_app),
                                      title: Text('Logout'),
                                      trailing: Icon(Icons.arrow_forward),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
