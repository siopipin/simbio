import 'package:flutter/material.dart';
import 'package:simbio/src/chat/ui/chat_beranda.dart';
import 'package:simbio/src/home/home_screen.dart';
import 'package:simbio/src/profile/profile_screen.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:simbio/src/widgets/umum_widget.dart';
import 'package:simbio/src/order/screens/orders_screen.dart';


class LandingScreen extends StatefulWidget {
  LandingScreen({Key key}) : super(key: key);

  _LandingScreenState createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> {
  @override
  void initState() { 
    super.initState();
  }

  int currentIndex = 0;
  final List<Widget> _bodyChildren = [
    HomeScreen(),
    OrderScreen(),
    ChatBeranda(),
    ProfileScreen()
  ];
  void onTabTapped(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: UmumWidget.appBar(title: "SIMBIO"),
      body: _bodyChildren[currentIndex],
      // drawer: UmumWidget.getDefaultDrawer(context),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: currentIndex,
        showElevation: true,
        onItemSelected: (index) => setState(() {
          currentIndex = index;
        }),
        items: [
          BottomNavyBarItem(
            icon: Icon(Icons.apps),
            title: Text('Beranda'),
            activeColor: COLOR.primary(),
          ),
          BottomNavyBarItem(
              icon: Icon(Icons.work),
              title: Text('Order'),
              activeColor: COLOR.primary()),
          BottomNavyBarItem(
              icon: Icon(Icons.message),
              title: Text('Chat'),
              activeColor: COLOR.primary()),
          BottomNavyBarItem(
              icon: Icon(Icons.person),
              title: Text('Profile'),
              activeColor: COLOR.primary()),
        ],
      ),
    );
  }
}
