import 'package:flutter/material.dart';
import 'package:simbio/src/util/pustaka.dart';

class CurveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(0, size.height);
    path.quadraticBezierTo(
        size.width / 2, size.height / 2, size.width, size.height);
    path.lineTo(size.width, 0);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

class ChildCurveContainer {
  static container(context) {
    return Container(
      width: SIZE.screen(context).width,
      height: SIZE.screen(context).height * .25,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            COLOR.background(),
            COLOR.primary(),
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
    );
  }
}
