import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:simbio/src/util/pustaka.dart';

class LoadingIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Center(
        child: CircularProgressIndicator(),
      );
}

class Loading {
  static spin(BuildContext context) {
    return SpinKitWave(color: COLOR.primary(), type: SpinKitWaveType.start);
  }
}
