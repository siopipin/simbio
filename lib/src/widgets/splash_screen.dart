import 'package:flutter/material.dart';
import 'package:simbio/src/util/pustaka.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: COLOR.primary(),
      body: new Center(
        child: Text(
          "Simbio v1.1",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
