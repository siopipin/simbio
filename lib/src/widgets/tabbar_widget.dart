import 'package:flutter/material.dart';
import 'package:simbio/src/util/pustaka.dart';

class TabbarWidget extends StatelessWidget {
  const TabbarWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: COLOR.primary(),
      title: Text("SIMBIO"),
      actions: <Widget>[
        Icon(
          Icons.notifications,
          size: 25,
        ),
        SizedBox(
          width: 10,
        )
      ],
      bottom: TabBar(
        tabs: <Widget>[
          Tab(
            text: 'Detail',
          ),
          Tab(
            text: 'Step',
          )
        ],
      ),
    );
  }
}
