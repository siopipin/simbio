import 'package:flutter/material.dart';
import 'package:simbio/src/landing/landing_screen.dart';
import 'package:simbio/src/produk/produk.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/order/screens/product_progress.dart';

class UmumWidget {
  static appBar({String title}) {
    return AppBar(
      title: new Text("$title"),
      backgroundColor: COLOR.background(),
      elevation: 0,
      // actions: <Widget>[
      //   Icon(
      //     Icons.notifications,
      //     size: 25,
      //   ),
      //   SizedBox(
      //     width: 10,
      //   )
      // ],
    );
  }

   static getDefaultDrawer(BuildContext context) {
    return new Drawer(
      elevation: 20.0,
      child: new ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(color: COLOR.background()),
            accountName: Text("Sio Jurnalis Pipin"),
            accountEmail: Text("siojurnalispipin@hotmail.com"),
            currentAccountPicture: CircleAvatar(
              backgroundColor: Theme.of(context).platform == TargetPlatform.iOS
                  ? Colors.blue
                  : Colors.white,
              child: Text(
                "S",
                style: TextStyle(fontSize: 40.0),
              ),
            ),
          ),
          new Container(
            color: Colors.white,
            child: new Column(
              children: <Widget>[
                ListTile(
                    leading: new Icon(Icons.dashboard),
                    title: Text("Dasboard"),
                    trailing: Icon(Icons.arrow_forward),
                    onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => LandingScreen()))),
                ListTile(
                  leading: new Icon(Icons.poll),
                  title: Text("Produk"),
                  trailing: Icon(Icons.arrow_forward),
                  onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ProdukScreen()))),
                new Divider(
                  height: 15.0,
                  color: Colors.grey[300],
                ),
                ListTile(
                    leading: new Icon(Icons.account_balance),
                    title: Text("Testing"),
                    trailing: new Icon(Icons.arrow_forward),
                    onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => ProductProgressUI())))
              ],
            ),
          )
        ],
      ),
    );
  }
}
