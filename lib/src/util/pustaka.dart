import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class URL {
  static api() => 'http://api.simbio.id:3200/mobile/';
  static socket() => 'http://api.simbio.id:3200/';
  static gambar() => 'http://simbio.id:3200/images/';
  static keyAuthFCM() =>
      'key=AAAAoZK9UUU:APA91bFIVXNXYtQwOyMwsF81IkRz1saWOQC8vk9Qyd_022lnVHTUkgLJIY7V0IpUJdo1FbYRetMcPJ6BxbAppsdGLr_1a-nMkAl-uiiaikZuyjysJRt5cQcH_sTK8fLAF6kGQj8yI1T9';
}

class COLOR {
  static background() => Color(0xff8DB81D);
  static primary() => Color(0xff8DB81D);
  static secondary() => new Color(0xffB3D55C);
  static ketiga() => new Color(0xff515A5A);
  static buildFriendColor() => new Color(0xffECEFF1);
  static linearone() => new Color(0xff388E3C);
  static lineartwo() => new Color(0xff81C784);
}

class SIZE {
  static screen(context) => MediaQuery.of(context).size;
}

class INFO {
  Future<String> token() async {
    final storage = new FlutterSecureStorage();
    String token = await storage.read(key: 'token');
    print('INFO.token: $token');
    return token;
  }
}
