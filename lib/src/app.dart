import 'package:flutter/material.dart';
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/auth/screens/auth_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simbio/src/landing/landing_screen.dart';
import 'package:simbio/src/widgets/loading_indicator.dart';
import 'package:simbio/src/widgets/splash_screen.dart';

import 'auth/bloc/authentication/bloc.dart';

class App extends StatelessWidget {
  final AuthRepository authRepository;

  App({Key key, @required this.authRepository}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var routes = <String, WidgetBuilder>{
      "/landing": (BuildContext context) => LandingScreen(),
    };

    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.light,
        backgroundColor: Color(0xfff7f8f9),
        fontFamily: 'Roboto',
        cardColor: Colors.white,
        accentColor: Color(0xffff1e39),
      ),
      debugShowCheckedModeBanner: false,
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          if (state is AuthenticationAuthenticated) {
            return LandingScreen();
          }
          if (state is AuthenticationUnauthenticated) {
            return AuthScreen();
          }
          if (state is AuthenticationLoading) {
            return LoadingIndicator();
          }
          return SplashScreen();
        },
      ),
      routes: routes,
    );
  }
}
