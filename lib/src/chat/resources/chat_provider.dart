import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' show Client, Response;
import 'package:meta/meta.dart';
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/chat/models/chat_list_model.dart';
import 'package:simbio/src/chat/models/chat_photo_list_model.dart';
import 'package:simbio/src/chat/models/chat_private_model.dart';
import 'package:simbio/src/chat/models/message_model.dart';
import 'package:simbio/src/chat/ui/chat_photo_list.dart';
import 'package:simbio/src/order/models/orders_model.dart';
import 'package:simbio/src/util/pustaka.dart';

class ChatProvider {
  Client client = Client();

  Future<MessageModel> getChatMessage(
      {@required int start, @required int limit}) async {
    Response response;
    final token = await AuthRepository().hasToken();
    final url = '${URL.api()}customer/chat-global/?start=$start&limit=$limit';
    response = await client
        .get(url, headers: {HttpHeaders.authorizationHeader: 'Barer $token'});

    final data = json.decode(response.body);
    // print('Chat_provider.dart / getChatMessage / data ${response.body}');
    if (response.statusCode == 200) {
      return MessageModel.fromJson(data);
    } else {
      throw Exception('Failed to load chat');
    }
  }

  Future<OrdersModel> getOrder() async {
    Response response;
    final token = await AuthRepository().hasToken();
    final String url = '${URL.api()}tekniker/orders';
    response = await client
        .get(url, headers: {HttpHeaders.authorizationHeader: 'Barer $token'});

    final data = json.decode(response.body);
    // print('Chat_provider.dart / getChatMessage / data ${response.body}');
    if (response.statusCode == 200) {
      return OrdersModel.fromJson(data);
    } else {
      throw Exception('Failed to load order');
    }
  }

  Future<ChatListModel> getChatList({@required int id}) async {
    Response response;
    final token = await AuthRepository().hasToken();
    final url = '${URL.api()}customer/chat-list/?userid=$id';
    response = await client
        .get(url, headers: {HttpHeaders.authorizationHeader: 'Barer $token'});

    final data = json.decode(response.body);
    print('chat_provider.dart / getChatList / url $url');
    print('chat_provider.dart / getChatList / data ${response.body}');
    if (response.statusCode == 200) {
      return ChatListModel.fromJson(data);
    } else {
      throw Exception('Failed to load chat');
    }
  }

  Future<MessageModel> getChatPrivate(
      {@required int channel, @required int start, @required int limit}) async {
    Response response;
    final token = await AuthRepository().hasToken();
    final url =
        '${URL.api()}customer/chat-private/?channel=$channel&start=$start&limit=$limit';
    response = await client
        .get(url, headers: {HttpHeaders.authorizationHeader: 'Barer $token'});

    final data = json.decode(response.body);
    print('chat_provider.dart / getChatPrivate / url $url');
    print('chat_provider.dart / getChatPrivate / data ${response.body}');
    if (response.statusCode == 200) {
      return MessageModel.fromJson(data);
    } else {
      throw Exception('Failed to load chat');
    }
  }

  Future<ChatPhotosListModel> getPhotoListGeneral(
      {@required int start, @required int limit}) async {
    Response response;
    final token = await AuthRepository().hasToken();
    final url =
        '${URL.api()}customer/chat-photo-global/?start=$start&limit=$limit';
    response = await client
        .get(url, headers: {HttpHeaders.authorizationHeader: 'Barer $token'});

    final data = json.decode(response.body);
    print('chat_provider.dart / getPhotoListGeneral / url $url');
    print('chat_provider.dart / getPhotoListGeneral / data ${response.body}');
    if (response.statusCode == 200) {
      return ChatPhotosListModel.fromJson(data);
    } else {
      throw Exception('Failed to load chat');
    }
  }

  Future<ChatPhotosListModel> getPhotoListPrivate(
      {@required int channel, @required int start, @required int limit}) async {
    Response response;
    final token = await AuthRepository().hasToken();
    final url =
        '${URL.api()}customer/chat-photo-private/?channel=$channel&start=$start&limit=$limit';
    response = await client
        .get(url, headers: {HttpHeaders.authorizationHeader: 'Barer $token'});

    final data = json.decode(response.body);
    print('chat_provider.dart / getPhotoListPrivate / url $url');
    print('chat_provider.dart / getPhotoListPrivate / data ${response.body}');
    if (response.statusCode == 200) {
      return ChatPhotosListModel.fromJson(data);
    } else {
      throw Exception('Failed to load chat');
    }
  }
}
