import 'dart:async';

import 'package:meta/meta.dart';
import 'package:simbio/src/chat/models/chat_list_model.dart';
import 'package:simbio/src/chat/models/chat_photo_list_model.dart';
import 'package:simbio/src/chat/models/chat_private_model.dart';
import 'package:simbio/src/chat/models/message_model.dart';
import 'package:simbio/src/chat/resources/chat_provider.dart';
import 'package:simbio/src/order/models/orders_model.dart';

class ChatRepository {
  ChatProvider chatProvider = ChatProvider();

  Future<MessageModel> getChatMessage(
          {@required int start, @required int limit}) =>
      chatProvider.getChatMessage(start: start, limit: limit);

  Future<ChatListModel> getChatList({@required int id}) =>
      chatProvider.getChatList(id: id);

  Future<MessageModel> getChatPrivate(
          {@required int channel, @required int start, @required int limit}) =>
      chatProvider.getChatPrivate(channel: channel, start: start, limit: limit);

  Future<ChatPhotosListModel> getPhotoListPrivate(
          {@required int channel, @required int start, @required int limit}) =>
      chatProvider.getPhotoListPrivate(
          channel: channel, start: start, limit: limit);

  Future<ChatPhotosListModel> getPhotoListGeneral(
          {@required int start, @required int limit}) =>
      chatProvider.getPhotoListGeneral(start: start, limit: limit);

  Future<OrdersModel> getOrder() => chatProvider.getOrder();
}
