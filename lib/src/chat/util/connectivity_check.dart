import 'package:data_connection_checker/data_connection_checker.dart';

class ConnectivityCheck {
  Future<bool> onConnection() async {
    bool result = await DataConnectionChecker().hasConnection;
    return result;
  }
}