import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:simbio/src/chat/models/chat_model.dart';
import 'package:simbio/src/chat/models/message_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

class ChatDBProvider {
  ChatDBProvider._();

  static final ChatDBProvider db = ChatDBProvider._();

  Database _database;

  Future<Database> get database async {
    print("DATABASE DULU");
    if (_database != null) return _database;
    _database = await getDatabaseInstance();
    return _database;
  }

  Future<Database> getDatabaseInstance({String namatbl}) async {
    print("BUAT TABEL $namatbl DULU");
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, "$namatbl.db");
    print('PANGGIL PATH : $path');
    return await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
          print('VERSI DB : $version');
      await db.execute(
        "CREATE TABLE $namatbl(id integer PRIMARY KEY AUTOINCREMENT, pengirim integer, tbl TEXT, room TEXT, channel integer, pesan TEXT, lampiran TEXT, date TEXT, nama TEXT, privilege TEXT, UNIQUE(date, pesan))",
      );
      print('TELAH BUAT TABEL : $namatbl');
    });
  }

  addChatToDatabase({ChatModel model, String namatbl}) async {
    final db = await database;
    var raw = await db.insert('$namatbl', model.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    return raw;
  }

  Future<List<ChatModel>> getAllChat({String namatbl}) async {
    final db = await database;
    var response = await db.query("$namatbl");
    List<ChatModel> list = response.map((c) => ChatModel.fromJson(c)).toList();
    print("chat_database / getAllChat / data list = ${list[0].nama}");
    return list;
  }

  deleteAllChat({String namatbl}) async {
    final db = await database;
    db.delete("$namatbl");
  }
}
