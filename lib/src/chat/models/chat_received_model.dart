class ReceivedChatModel {
  int insertId;
  dynamic image;

  ReceivedChatModel({this.insertId, this.image});

  ReceivedChatModel.fromJson(Map<String, dynamic> json) {
    insertId = json['insertId'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['insertId'] = this.insertId;
    data['image'] = this.image;
    return data;
  }
}