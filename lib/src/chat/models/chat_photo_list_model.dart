class ChatPhotosListModel {
  int status;
  List<Data> data;

  ChatPhotosListModel({this.status, this.data});

  ChatPhotosListModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String lampiran;
  String tbl;
  String date;
  int pengirim;
  String pesan;
  String nama;
  String privilege;
  int dateNumber;

  Data(
      {this.lampiran,
      this.tbl,
      this.date,
      this.pengirim,
      this.pesan,
      this.nama,
      this.privilege,
      this.dateNumber});

  Data.fromJson(Map<String, dynamic> json) {
    lampiran = json['lampiran'];
    tbl = json['tbl'];
    date = json['date'];
    pengirim = json['pengirim'];
    pesan = json['pesan'];
    nama = json['nama'];
    privilege = json['privilege'];
    dateNumber = json['date_number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['lampiran'] = this.lampiran;
    data['tbl'] = this.tbl;
    data['date'] = this.date;
    data['pengirim'] = this.pengirim;
    data['pesan'] = this.pesan;
    data['nama'] = this.nama;
    data['privilege'] = this.privilege;
    data['date_number'] = this.dateNumber;
    return data;
  }
}