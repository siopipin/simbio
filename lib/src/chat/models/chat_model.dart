class ChatModel {
  int pengirim;
  String tbl;
  String room;
  int channel;
  String pesan;
  dynamic lampiran;
  String date;
  String nama;
  String privilege;

  ChatModel(
      {this.pengirim,
      this.tbl,
      this.room,
      this.channel,
      this.pesan,
      this.lampiran,
      this.date,
      this.nama,
      this.privilege});

  ChatModel.fromJson(Map<String, dynamic> json) {
    pengirim = json['pengirim'];
    tbl = json['tbl'];
    room = json['room'];
    channel = json['channel'];
    pesan = json['pesan'];
    lampiran = json['lampiran'];
    date = json['date'];
    nama = json['nama'];
    privilege = json['privilege'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pengirim'] = this.pengirim;
    data['tbl'] = this.tbl;
    data['room'] = this.room;
    data['channel'] = this.channel;
    data['pesan'] = this.pesan;
    data['lampiran'] = this.lampiran;
    data['date'] = this.date;
    data['nama'] = this.nama;
    data['privilege'] = this.privilege;
    return data;
  }
}
