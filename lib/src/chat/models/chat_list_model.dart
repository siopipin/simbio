class ChatListModel {
  int status;
  List<Data> data;

  ChatListModel({this.status, this.data});

  ChatListModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int channel;
  String room;
  String nama;
  String customer;
  String namaPasien;
  String pesan;
  String date;
  int dateNumber;

  Data(
      {this.channel,
      this.room,
      this.nama,
      this.customer,
      this.namaPasien,
      this.pesan,
      this.date,
      this.dateNumber});

  Data.fromJson(Map<String, dynamic> json) {
    channel = json['channel'];
    room = json['room'];
    nama = json['nama'];
    customer = json['customer'];
    namaPasien = json['nama_pasien'];
    pesan = json['pesan'];
    date = json['date'];
    dateNumber = json['date_number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['channel'] = this.channel;
    data['room'] = this.room;
    data['nama'] = this.nama;
    data['customer'] = this.customer;
    data['nama_pasien'] = this.namaPasien;
    data['pesan'] = this.pesan;
    data['date'] = this.date;
    data['date_number'] = this.dateNumber;
    return data;
  }
}