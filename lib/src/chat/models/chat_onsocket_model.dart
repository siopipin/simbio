class ChatOnSocketModel {
  User user;
  Message message;
  String date;
  int dateNumber;

  ChatOnSocketModel({this.user, this.message, this.date, this.dateNumber});

  ChatOnSocketModel.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    message =
        json['message'] != null ? new Message.fromJson(json['message']) : null;
    date = json['date'];
    dateNumber = json['date_number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.message != null) {
      data['message'] = this.message.toJson();
    }
    data['date'] = this.date;
    data['date_number'] = this.dateNumber;
    return data;
  }
}

class User {
  int userId;
  String name;
  String privilege;
  dynamic token;

  User({this.userId, this.name, this.privilege, this.token});

  User.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    name = json['name'];
    privilege = json['privilege'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['name'] = this.name;
    data['privilege'] = this.privilege;
    data['token'] = this.token;
    return data;
  }
}

class Message {
  int pengirim;
  String tbl;
  String room;
  int channel;
  String pesan;
  dynamic lampiran;
  int id;

  Message(
      {this.pengirim,
      this.tbl,
      this.room,
      this.channel,
      this.pesan,
      this.lampiran,
      this.id});

  Message.fromJson(Map<String, dynamic> json) {
    pengirim = json['pengirim'];
    tbl = json['tbl'];
    room = json['room'];
    channel = json['channel'];
    pesan = json['pesan'];
    lampiran = json['lampiran'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pengirim'] = this.pengirim;
    data['tbl'] = this.tbl;
    data['room'] = this.room;
    data['channel'] = this.channel;
    data['pesan'] = this.pesan;
    data['lampiran'] = this.lampiran;
    data['id'] = this.id;
    return data;
  }
}