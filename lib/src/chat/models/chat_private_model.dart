class ChatPrivateModel {
  int status;
  List<Data> data;

  ChatPrivateModel({this.status, this.data});

  ChatPrivateModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int pengirim;
  String tbl;
  String room;
  int channel;
  String pesan;
  dynamic lampiran;
  String date;
  String nama;
  String privilege;
  int dateNumber;

  Data(
      {this.pengirim,
      this.tbl,
      this.room,
      this.channel,
      this.pesan,
      this.lampiran,
      this.date,
      this.nama,
      this.privilege,
      this.dateNumber});

  Data.fromJson(Map<String, dynamic> json) {
    pengirim = json['pengirim'];
    tbl = json['tbl'];
    room = json['room'];
    channel = json['channel'];
    pesan = json['pesan'];
    lampiran = json['lampiran'];
    date = json['date'];
    nama = json['nama'];
    privilege = json['privilege'];
    dateNumber = json['date_number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pengirim'] = this.pengirim;
    data['tbl'] = this.tbl;
    data['room'] = this.room;
    data['channel'] = this.channel;
    data['pesan'] = this.pesan;
    data['lampiran'] = this.lampiran;
    data['date'] = this.date;
    data['nama'] = this.nama;
    data['privilege'] = this.privilege;
    data['date_number'] = this.dateNumber;
    return data;
  }
}
