import 'package:flutter/material.dart';
import 'package:simbio/src/chat/blocs/chat_bloc.dart';
import 'package:simbio/src/chat/models/chat_photo_list_model.dart';
import 'package:simbio/src/chat/ui/widgets/chat_image_card.dart';
import 'package:simbio/src/util/pustaka.dart';

class ChatPhotoList extends StatefulWidget {
  ChatPhotoList({Key key}) : super(key: key);

  _ChatPhotoListState createState() => _ChatPhotoListState();
}

class _ChatPhotoListState extends State<ChatPhotoList> {
  int start = 0;
  int limit = 10;
  ScrollController scrollController = new ScrollController();
  ChatPhotosListModel snapshot;
  final List<ChatImageCard> photos = <ChatImageCard>[];
  @override
  void initState() {
    super.initState();
    // chatBLOC.getPhotoListGeneral(limit: limit, start: start);
    getImageList();
    infiniteChatCek();
  }

  infiniteChatCek() {
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        print("In MaxScroll Extend");
        start += limit;
        reGetImageList();
      }
    });
  }

  getImageList() async {
    snapshot = await chatBLOC.getPhotoListGeneral(limit: limit, start: start);

    for (int i = snapshot.data.length - 1; i >= 0; i--) {
      ChatImageCard data = new ChatImageCard(
        date: "${snapshot.data[i].date}",
        gambar: "${snapshot.data[i].lampiran}",
        nama: "${snapshot.data[i].nama}",
        pesan: "${snapshot.data[i].pesan}",
      );
      setState(() {
        photos.add(data);
      });
    }
  }

  reGetImageList() async {
    snapshot = await chatBLOC.getPhotoListGeneral(limit: limit, start: start);

    for (int i = snapshot.data.length - 1; i >= 0; i--) {
      ChatImageCard data = new ChatImageCard(
        date: "${snapshot.data[i].date}",
        gambar: "${snapshot.data[i].lampiran}",
        nama: "${snapshot.data[i].nama}",
        pesan: "${snapshot.data[i].pesan}",
      );
      setState(() {
        photos.add(data);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: appBar(context), body: listPhoto(context));
  }

  Widget appBar(BuildContext context) {
    return AppBar(
      title: Container(
        child: Row(
          children: <Widget>[
            Icon(Icons.photo_library),
            SizedBox(
              width: 6,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Photos",
                  style: TextStyle(fontSize: 15),
                ),
                Text("All photos",
                    style: TextStyle(fontSize: 11, color: Colors.white70)),
              ],
            )
          ],
        ),
      ),
      backgroundColor: COLOR.background(),
    );
  }

  Widget listPhoto(BuildContext context) {
    return photos.length <= 0
        ? Center(
            child: CircularProgressIndicator(),
          )
        : Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: ListView.builder(
              itemCount: 1,
              controller: scrollController,
              itemBuilder: (BuildContext context, int index) {
                return GridView.count(
                    physics: ClampingScrollPhysics(),
                    crossAxisCount: 2,
                    shrinkWrap: true,
                    children: List.generate(photos.length, (i) {
                      return photos[i];
                    }));
              },
            ),
          );
  }
}
