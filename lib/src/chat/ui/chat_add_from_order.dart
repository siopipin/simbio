import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:simbio/src/chat/blocs/chat_bloc.dart';
import 'package:simbio/src/chat/ui/chat_private.dart';
import 'package:simbio/src/order/models/orders_model.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/widgets/umum_widget.dart';

class ChatAddFromOrderUI extends StatefulWidget {
  ChatAddFromOrderUI({Key key}) : super(key: key);

  _ChatAddFromOrderUIState createState() => _ChatAddFromOrderUIState();
}

class _ChatAddFromOrderUIState extends State<ChatAddFromOrderUI> {
  @override
  void initState() {
    super.initState();
    chatBLOC.doGetOrder();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: UmumWidget.appBar(title: "Tambah Chat"),
      body: StreamBuilder(
        stream: chatBLOC.streamOrder,
        builder: (BuildContext context, AsyncSnapshot<OrdersModel> snapshot) {
          if (snapshot.hasData) {
            return Container(
                child: Scrollbar(
              child: ListView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: snapshot.data.orders.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    child: GestureDetector(
                      onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ChatPrivate(
                                    idOrder: snapshot.data.orders[index].id
                                        .toString(),
                                    customer:
                                        snapshot.data.orders[index].customer,
                                    pasien:
                                        snapshot.data.orders[index].namaPasien,
                                    idOrderString:
                                        snapshot.data.orders[index].idOrder,
                                  ))),
                      child: Card(
                        child: ListTile(
                          title: Text(
                              "${snapshot.data.orders[index].customer} #${snapshot.data.orders[index].idOrder}"),
                          subtitle:
                              Text(snapshot.data.orders[index].namaPasien),
                          trailing: Icon(Icons.chat),
                        ),
                      ),
                    ),
                    padding: EdgeInsets.only(left: 10, right: 10),
                  );
                },
              ),
            ));
          } else if (snapshot.hasError) {
            return Center(
              child: Text(snapshot.error.toString()),
            );
          } else {
            return SpinKitWave(
                color: COLOR.primary(), type: SpinKitWaveType.start);
          }
        },
      ),
    );
  }
}
