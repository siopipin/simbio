import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:simbio/src/chat/ui/widgets/chat_image_fullscreen.dart';
import 'package:simbio/src/util/formatter_util.dart';
import 'package:simbio/src/util/pustaka.dart';

class ChatImageCard extends StatefulWidget {
  final String date;
  final String gambar;
  final String nama;
  final String pesan;

  ChatImageCard({this.date, this.gambar, this.nama, this.pesan, Key key})
      : super(key: key);

  _ChatImageCardState createState() => _ChatImageCardState();
}

class _ChatImageCardState extends State<ChatImageCard> {
  @override
  Widget build(BuildContext context) {
    String tanggal = formaterDate.format(DateTime.parse(widget.date));
    String pesan;
    if (widget.pesan.length > 100) {
      pesan = widget.pesan.substring(0, 100);
    } else {
      pesan = widget.pesan;
    }
    return GestureDetector(
        onTap: () {
          return Navigator.push(context, MaterialPageRoute(builder: (context) {
            return ChatImageFullScreen(
              imageProvider:
                  CachedNetworkImageProvider('${URL.gambar()}${widget.gambar}'),
            );
          }));
        },
        child: Container(
          padding: EdgeInsets.all(10),
          width: 300,
          height: 300,
          child: Stack(
            alignment: Alignment.bottomLeft,
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                child: Card(
                  semanticContainer: true,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  child: Image.network(
                    '${URL.gambar()}${widget.gambar}',
                    fit: BoxFit.fill,
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  margin: EdgeInsets.all(10),
                ),
              ),
              Container(
                color: Colors.black.withOpacity(0.5),
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text("${widget.nama} | ",
                            style: TextStyle(
                                fontSize: 13,
                                fontWeight: FontWeight.bold,
                                color: Colors.white)),
                        Text("($tanggal)",
                            style:
                                TextStyle(fontSize: 10, color: Colors.white)),
                      ],
                    ),
                    Text("$pesan",
                        style: TextStyle(fontSize: 12, color: Colors.white)),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
