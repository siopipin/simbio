import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:path_provider/path_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:simbio/src/chat/ui/widgets/chat_image_fullscreen.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'dart:io' as Io;
import 'package:timeago/timeago.dart' as timeago;

class ChatMessage extends StatefulWidget {
  final String privilege;
  final String idTekniker;
  final String name;
  final String text;
  final String id;
  final String time;
  final String tbl;
  final File img;
  final String imgfromsocket;
  final String imgFromdb;
  final String filepath;
  ChatMessage(
      {this.privilege,
      this.filepath,
      this.idTekniker,
      this.text,
      this.img,
      this.imgFromdb,
      this.id,
      this.time,
      this.imgfromsocket,
      this.name,
      this.tbl,
      Key key})
      : super(key: key);

  _ChatMessageState createState() => _ChatMessageState();
}

class _ChatMessageState extends State<ChatMessage> {
  String idTekniker;
  // Uint8List _byteImage;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.id == widget.idTekniker && widget.tbl == "pegawai"
        ? _buildSelfMessage(
            filepath: widget.filepath,
            context: context,
            text: widget.text,
            img: widget.img,
            time: widget.time,
            imgFromDB: widget.imgFromdb)
        : _buildFriendMessage(
            context: context,
            text: widget.text,
            time: widget.time,
            privilege: widget.privilege,
            imgfromdb: widget.imgFromdb,
            imgfromsocket: widget.imgfromsocket);
  }

  Widget _buildSelfMessage(
      {BuildContext context,
      String text,
      String time,
      File img,
      String filepath,
      String imgFromDB}) {
    String perbedaanwaktu = formatWaktu(time);
    return Container(
      constraints:
          BoxConstraints(maxWidth: MediaQuery.of(context).size.width / 2 + 20),
      padding: EdgeInsets.symmetric(vertical: 7.5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(10),
                  bottomLeft: Radius.circular(15),
                ),
                child: Container(
                    color: COLOR.primary(),
                    padding: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width / 2 + 50,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        img != null
                            ? checkImageFile(context: context, file: img)
                            : Container(),
                        checkImagesFromDBToShow(
                            context: context, imgfromdb: imgFromDB),
                        Text(
                          text,
                          style: TextStyle(
                            color: Colors.white,
                            height: 1.2,
                          ),
                        ),
                      ],
                    )),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text(perbedaanwaktu,
                  style: TextStyle(
                    color: Colors.grey,
                    height: 1.5,
                    fontSize: 10,
                  )),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildFriendMessage(
      {BuildContext context,
      String text,
      String time,
      String privilege,
      String imgfromsocket,
      String imgfromdb}) {
    String perbedaanwaktu = formatWaktu(time);
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 7.5),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(10),
                      bottomLeft: Radius.circular(15),
                    ),
                    child: Container(
                        color: COLOR.buildFriendColor(),
                        padding: EdgeInsets.all(10),
                        width: MediaQuery.of(context).size.width / 2 + 50,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            (imgfromsocket != null)
                                ? checkImagesReadyToShow(
                                    context: context,
                                    imgfromsocket: imgfromsocket)
                                : (imgfromdb != null)
                                    ? checkImagesFromDBToShow(
                                        context: context, imgfromdb: imgfromdb)
                                    : Container(),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                      widget.name != null
                                          ? widget.name
                                          : "Admin",
                                      style: TextStyle(
                                        color: COLOR.primary(),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14,
                                      )),
                                  Text(
                                      widget.privilege != null
                                          ? widget.privilege
                                          : "Privilege",
                                      style: TextStyle(
                                        color: COLOR.ketiga(),
                                        fontSize: 11,
                                      ))
                                ],
                              ),
                            ),
                            Divider(),
                            Text(
                              text,
                              style: TextStyle(
                                color: Colors.grey,
                                height: 1.2,
                              ),
                            ),
                          ],
                        )),
                  ),
                ],
              )
            ],
          ),
          Padding(
            padding: EdgeInsets.only(left: 3),
            child: Text(
              perbedaanwaktu,
              style: TextStyle(
                color: Colors.grey,
                height: 1.5,
                fontSize: 10,
              ),
            ),
          ),
        ],
      ),
    );
  }

  checkImagesReadyToShow({BuildContext context, String imgfromsocket}) {
    print('I AM HERE TO SHOW IMG FROM SOCKET : $imgfromsocket');
    Uint8List bytes = Base64Decoder().convert(imgfromsocket);
    return (imgfromsocket == null || imgfromsocket == 'null') ? Container() : (imgfromsocket != null || imgfromsocket != 'null') ? GestureDetector(
        onTap: () {
          return Navigator.push(context, MaterialPageRoute(builder: (context) {
            return ChatImageFullScreenFromBase64(
              imageProvider: MemoryImage(bytes),
            );
          }));
        },
        child: Container(child: new Image.memory(bytes))) : Container();
  }

  checkImagesFromDBToShow({BuildContext context, String imgfromdb}) {
    return imgfromdb == null || imgfromdb == "null"
        ? Container()
        : imgfromdb != null || imgfromdb != "null"
            ? GestureDetector(
                onTap: () {
                  return Navigator.push(context,
                      MaterialPageRoute(builder: (context) {
                    return ChatImageFullScreen(
                      imageProvider: CachedNetworkImageProvider(
                          '${URL.gambar()}$imgfromdb'),
                    );
                  }));
                },
                child: Container(
                    padding: EdgeInsets.all(5),
                    child: Image(
                        image: CachedNetworkImageProvider(
                            '${URL.gambar()}$imgfromdb'))))
            : Container();
  }

  checkImageFile({BuildContext context, File file}) {
    return GestureDetector(
        onTap: () {
          return Navigator.push(context, MaterialPageRoute(builder: (context) {
            return ChatImageFullScreenFromFilePath(
              imageProvider: FileImage(File('${file.path}')),
            );
          }));
        },
        child: Container(child: Image.file(file)));
  }

  checkImagesSelftMessageFromDBToShow(
      {BuildContext context, String imgfromdb}) {
    return imgfromdb == null || imgfromdb == "null"
        ? Container()
        : imgfromdb != null || imgfromdb != "null"
            ? GestureDetector(
                onTap: () {
                  return Navigator.push(context,
                      MaterialPageRoute(builder: (context) {
                    return ChatImageFullScreen(
                      imageProvider: CachedNetworkImageProvider(
                          '${URL.gambar()}$imgfromdb'),
                    );
                  }));
                },
                child: Container(
                    padding: EdgeInsets.all(5),
                    child: Image(
                        image: CachedNetworkImageProvider(
                            '${URL.gambar()}$imgfromdb'))))
            : Container();
  }

  formatWaktu(String time) {
    final now = new DateTime.now();
    final difference = now.difference(DateTime.parse(time));
    return timeago.format(now.subtract(difference), locale: 'en');
  }
}
