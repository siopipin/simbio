import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter_socket_io/flutter_socket_io.dart';
import 'package:flutter_socket_io/socket_io_manager.dart';
import 'package:image_picker/image_picker.dart';
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/chat/blocs/chat_bloc.dart';
import 'package:simbio/src/chat/blocs/chat_fcm_general.dart';
import 'package:simbio/src/chat/models/chat_model.dart';
import 'package:simbio/src/chat/models/chat_onsocket_model.dart';
import 'package:simbio/src/chat/models/message_model.dart';
import 'package:simbio/src/chat/ui/chat_on_db.dart';
import 'package:simbio/src/chat/ui/chat_photo_list.dart';
import 'package:simbio/src/chat/ui/widgets/chat_message.dart';
import 'package:simbio/src/chat/util/chat_database.dart';
import 'package:simbio/src/chat/util/connectivity_check.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/util/toas.dart';
import 'package:image/image.dart' as ImageProcess;
import 'package:toast/toast.dart';

class GeneralChatScreen extends StatefulWidget {
  @override
  State createState() => new GeneralChatScreenState();
}

class GeneralChatScreenState extends State<GeneralChatScreen> {
  ScrollController _scrollController = new ScrollController();
  final TextEditingController _chatController = new TextEditingController();
  final List<ChatMessage> _messages = <ChatMessage>[];
  SocketIO socketIO;
  var id;
  var name;
  var privilage;
  var token;
  int start = 0;
  int limit = 10;
  bool isLoading = false;
  MessageModel chatData;
  bool statusInternet;
  var tokenOnSocket;

  //ImagePicker
  final TextEditingController _chatControllerInPicture =
      new TextEditingController();
  String base64Image;
  File file;
  Uint8List _byteImage;

  @override
  void initState() {
    super.initState();
    cekInternet();
    new ChatFCMGeneral().setUpFirebase(context);
  }

  cekInternet() async {
    statusInternet = await ConnectivityCheck().onConnection();
    setState(() {
      statusInternet = statusInternet;
      if (statusInternet == true) {
        if (statusInternet == true) {
          setState(() {
            showChatFromServer();
            connectToSocket();
            infiniteChatCek();
          });
        }
      }
    });
  }

  infiniteChatCek() {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        print("In MaxScroll Extend");
        start += limit;
        isLoading = true;
        showChatFromServer();
      }
    });
  }

  showChatFromServer() async {
    // await ChatDBProvider.db.getDatabaseInstance(namatbl: "chat");
    id = await AuthRepository().hasId();
    chatData = await chatBLOC.doGetChat(start: start, limit: limit);
    for (int i = chatData.data.length - 1; i >= 0; i--) {
      ChatMessage messageData = new ChatMessage(
        idTekniker: id,
        text: "${chatData.data[i].pesan}",
        id: "${chatData.data[i].pengirim}",
        time: "${chatData.data[i].date}",
        name: "${chatData.data[i].nama}",
        tbl: "${chatData.data[i].tbl}",
        privilege: "${chatData.data[i].privilege}",
        imgFromdb: "${chatData.data[i].lampiran}",
      );

      // await ChatDBProvider.db.addChatToDatabase(
      //     model: ChatModel(
      //         pengirim: chatData.data[i].pengirim,
      //         tbl: "${chatData.data[i].tbl}",
      //         room: "${chatData.data[i].room}",
      //         channel: chatData.data[i].channel,
      //         pesan: "${chatData.data[i].pesan}",
      //         lampiran: "${chatData.data[i].lampiran}",
      //         date: "${chatData.data[i].date}",
      //         nama: "${chatData.data[i].nama}",
      //         privilege: "${chatData.data[i].privilege}"),
      //     namatbl: 'chat');
      setState(() {
        _messages.add(messageData);
      });
    }
  }

  connectToSocket() async {
    id = await AuthRepository().hasId();
    name = await AuthRepository().hasName();
    privilage = await AuthRepository().hasPrivilege();
    token = await AuthRepository().hasToken();
    String data =
        '{ userId : $id, name : "$name", privilege : $privilage, token : $token}';
    String url = '${URL.socket()}';
    socketIO = SocketIOManager()
        .createSocketIO(url, "/", socketStatusCallback: _socketStatus);
    socketIO.init();
    socketIO.subscribe("new message", oncomingChat);
    socketIO.connect();
    socketIO.sendMessage("new user", data, _onReceiveChatMessage);
  }

  _socketStatus(dynamic data) {
    print("Socket status: " + data);
  }

  oncomingChat(dynamic data) {
    var datas = json.decode(data);
    print("chat_screen / oncomingChat / message : $datas");
    var res = ChatOnSocketModel.fromJson(datas);
    if (res.message.lampiran != null) {
      showChatOnSocketWithImage(res);
    } else {
      showChatOnSocketWithoutImage(res);
    }
  }

  cekTokenOnSocket(ChatOnSocketModel data) {
    if (data.user.token == null) {
      setState(() {
        tokenOnSocket = 'admin';
      });
    } else {
      setState(() {
        tokenOnSocket = data.user.token;
      });
    }
    print("Status TOKEN ON SOCKET : $tokenOnSocket");
  }

  showChatOnSocketWithImage(ChatOnSocketModel data) {
    String gambartmp = data.message.lampiran;
    String gambardarisocket;
    const start = "base64,";
    final startIndex = gambartmp.indexOf(start);
    gambardarisocket =
        gambartmp.substring(startIndex + start.length, gambartmp.length);
    ChatMessage messageData = new ChatMessage(
      text: "${data.message.pesan}",
      id: "${data.user.userId}",
      time: "${data.date}",
      name: "${data.user.name}",
      privilege: "${data.user.privilege}",
      tbl: "${data.message.tbl}",
      imgfromsocket: "$gambardarisocket",
    );
    setState(() {
      _messages.insert(0, messageData);
    });
    cekTokenOnSocket(data);
  }

  showChatOnSocketWithoutImage(ChatOnSocketModel data) {
    ChatMessage messageData = new ChatMessage(
      text: "${data.message.pesan}",
      id: "${data.user.userId}",
      time: "${data.date}",
      name: "${data.user.name}",
      privilege: "${data.user.privilege}",
      tbl: "${data.message.tbl}",
    );
    setState(() {
      _messages.insert(0, messageData);
    });
    cekTokenOnSocket(data);
  }

  void _onReceiveChatMessage(dynamic data) {
    print("chat_screen / onReceiveChatMessage / message : $data");
    var datas = json.decode(data);
    print(datas);
  }

  void sendChatMessage(String msg) async {
    print("_chat / _sendChatMessage / msg : $msg");
    if (socketIO != null) {
      String data =
          '{ userId : $id, name : "$name", privilege : $privilage, token : $token}';
      socketIO.sendMessage("new user", data, _onReceiveChatMessage);
      String jsonData =
          '{"pengirim": $id, "tbl": "pegawai", "room" : "general", "channel" : $id, "pesan" : "$msg", "lampiran" : null}';
      socketIO.sendMessage("new message", jsonData, _onReceiveChatMessage);
    }
  }

  void sendChatMessageWithPicture(String msg) async {
    if (socketIO != null) {
      String datas =
          '{ userId : $id, name : "$name", privilege : $privilage, token : $token}';
      socketIO.sendMessage("new user", datas, _onReceiveChatMessage);
      String data =
          '{"pengirim": $id, "tbl": "pegawai", "room" : "general", "channel" : $id, "pesan" : "$msg", "lampiran" : "data:image/png;base64,$base64Image"}';
      socketIO.sendMessage("new message", data, _onReceiveChatMessage);
    }
  }

  void handleSubmitChatWitoutImage(String text) async {
    _chatController.clear();
    DateTime nowTime = DateTime.now();
    ChatMessage message = new ChatMessage(
      idTekniker: id,
      text: text,
      id: id,
      time: nowTime.toString(),
      name: name,
      tbl: "pegawai",
    );

    setState(() {
      _messages.insert(0, message);
    });
  }

  sendNotification(String pesan) async {
    await ChatFCMGeneral().sendNotificationChat(
        pengirim: name, topic: tokenOnSocket, pesan: pesan);
  }

  imageSelectorGallery() async {
    file = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 80);
    await convertToBase64(file);
    setState(() {
      kirimPesanGambar(file);
    });
  }

  imageSelectorCamera() async {
    file = await ImagePicker.pickImage(
        source: ImageSource.camera, maxWidth: 500, maxHeight: 900);
    await convertToBase64(file);
    setState(() {
      kirimPesanGambar(file);
    });
  }

  convertToBase64(File file) async {
    final _imageFile = ImageProcess.decodeImage(
      file.readAsBytesSync(),
    );
    base64Image = base64Encode(ImageProcess.encodePng(_imageFile));
  }

  Future<void> kirimPesanGambar(File file) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: new SingleChildScrollView(
              child: new ListBody(
                children: <Widget>[
                  header(context),
                  displaySelectedFile(context, file),
                  _chatEnvironmentPhoto()
                ],
              ),
            ),
          );
        });
  }

  void _handleSubmitPicture(String text) {
    _chatControllerInPicture.clear();
    DateTime nowTime = DateTime.now();

    ChatMessage message = new ChatMessage(
        idTekniker: id,
        text: text,
        id: id,
        time: nowTime.toString(),
        name: name,
        tbl: "pegawai",
        img: file);
    setState(() {
      _messages.insert(0, message);
    });
  }

  Widget displaySelectedFile(BuildContext context, File file) {
    double height = MediaQuery.of(context).size.height / 3;
    double width = MediaQuery.of(context).size.width / 2 + 50;
    return Container(
      child: Stack(
        children: <Widget>[
          SizedBox(
            height: height,
            width: width,
            child: file == null
                ? new Center(
                    child: new Icon(
                      Icons.photo_camera,
                      size: 100,
                      color: Colors.black54,
                    ),
                  )
                : new Container(
                    padding: EdgeInsets.all(5),
                    child: Image.file(file),
                  ),
          ),
        ],
      ),
    );
  }

  Widget header(BuildContext context) => Ink(
        decoration: BoxDecoration(
            gradient: LinearGradient(
          colors: [
            COLOR.secondary(),
            COLOR.primary(),
          ],
        )),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Icon(
                  Icons.photo_album,
                  color: Colors.white,
                  size: 27,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Kirim Pesan Gambar",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                    Text(
                      "Preview Gambar",
                      style: TextStyle(color: Colors.white60, fontSize: 13),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      );

  Widget _chatEnvironment() {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          border: BorderDirectional(
            top: BorderSide(
              color: Colors.grey,
              width: 0.5,
            ),
          ),
        ),
        child: Align(
          alignment: FractionalOffset.bottomCenter,
          child: Row(
            children: <Widget>[
              Expanded(
                child: TextFormField(
                  controller: _chatController,
                  maxLines: null,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 25),
                    hintText: "Type a message",
                    hintStyle: TextStyle(
                      color: Colors.black38,
                      fontSize: 14,
                    ),
                  ),
                  style: TextStyle(
                    color: Colors.black54,
                    fontSize: 12,
                  ),
                ),
              ),
              IconButton(
                icon: Icon(Icons.image, color: Colors.grey),
                onPressed: imageSelectorGallery,
              ),
              IconButton(
                icon: Icon(Icons.photo_camera, color: Colors.grey),
                onPressed: imageSelectorCamera,
              ),
              GestureDetector(
                onTap: () {
                  print(
                      "chat_screen / _chatEnv / isi pesan : ${_chatController.text}");
                  if (_chatController.text.length > 0) {
                    sendNotification(_chatController.text);
                    sendChatMessage(_chatController.text);
                    handleSubmitChatWitoutImage(_chatController.text);
                  } else {
                    return showToast("Pesan tidak boleh kosong", context,
                        duration: 2, gravity: Toast.BOTTOM);
                  }
                },
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 8),
                  margin: EdgeInsets.only(right: 20, left: 5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: COLOR.primary(),
                  ),
                  child: Icon(
                    Icons.send,
                    color: Colors.white,
                    size: 18,
                  ),
                ),
              )
            ],
          ),
        ));
  }

  Widget _chatEnvironmentPhoto() {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          border: BorderDirectional(
            top: BorderSide(
              color: Colors.grey,
              width: 0.5,
            ),
          ),
        ),
        child: Align(
          alignment: FractionalOffset.bottomCenter,
          child: Row(
            children: <Widget>[
              Expanded(
                child: TextFormField(
                  controller: _chatControllerInPicture,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 25),
                    hintText: "Type a message",
                    hintStyle: TextStyle(
                      color: Colors.black38,
                      fontSize: 14,
                    ),
                  ),
                  style: TextStyle(
                    color: Colors.black54,
                    fontSize: 12,
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  print(
                      "chat_scree / _chatEnvSendImage / isi pesan : ${_chatController.text}");
                  if (_chatControllerInPicture.text.length > 0) {
                    sendChatMessageWithPicture(_chatControllerInPicture.text);
                    _handleSubmitPicture(_chatControllerInPicture.text);
                    sendNotification(_chatControllerInPicture.text);
                    Navigator.pop(context);
                  } else {
                    return showToast("Pesan tidak boleh kosong", context,
                        duration: 2, gravity: Toast.BOTTOM);
                  }
                },
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 8),
                  margin: EdgeInsets.only(right: 20, left: 5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: COLOR.primary(),
                  ),
                  child: Icon(
                    Icons.send,
                    color: Colors.white,
                    size: 18,
                  ),
                ),
              )
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    print("Status INTERNET : $statusInternet");
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          title: Container(
            child: Row(
              children: <Widget>[
                Icon(Icons.widgets),
                SizedBox(
                  width: 6,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "General Chat",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text("Ruang chat untuk admin",
                        style: TextStyle(fontSize: 11, color: Colors.white70)),
                  ],
                )
              ],
            ),
          ),
          backgroundColor: COLOR.background(),
          elevation: 0,
          actions: <Widget>[
            SizedBox(
              width: 10,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ChatPhotoList()));
              },
              child: Icon(
                Icons.photo_library,
                size: 25,
              ),
            ),
            SizedBox(
              width: 10,
            )
          ],
        ),
        body: Column(
          children: <Widget>[
            new Flexible(
              child: ListView.builder(
                controller: _scrollController,
                padding: new EdgeInsets.all(8.0),
                reverse: true,
                itemBuilder: (_, int index) => _messages[index],
                itemCount: _messages.length,
              ),
            ),
            new Divider(
              height: 1.0,
            ),
            new Container(
              decoration: new BoxDecoration(
                color: Theme.of(context).cardColor,
              ),
              child: _chatEnvironment(),
            ),
          ],
        ));
  }
}
