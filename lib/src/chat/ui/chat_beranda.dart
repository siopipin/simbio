import 'package:flutter/material.dart';
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/chat/blocs/chat_bloc.dart';
import 'package:simbio/src/chat/models/chat_list_model.dart';
import 'package:simbio/src/chat/ui/chat_add_from_order.dart';
import 'package:simbio/src/chat/ui/chat_private.dart';
import 'package:simbio/src/chat/ui/chat_general.dart';
import 'package:simbio/src/util/firebase_notification_handler.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/widgets/curver_clipper_widget.dart';
import 'package:simbio/src/widgets/loading_indicator.dart';

class ChatBeranda extends StatefulWidget {
  ChatBeranda({Key key}) : super(key: key);

  _ChatBerandaState createState() => _ChatBerandaState();
}

class _ChatBerandaState extends State<ChatBeranda> {
  @override
  void initState() {
    super.initState();
    new FirebaseNotifications().setUpFirebase(context);
    initGetID();
  }

  initGetID() async {
    String tmpid = await AuthRepository().hasId();
    int id = int.parse(tmpid);
    chatBLOC.doGetChatList(id: id);
    print("chat_beranda / initGetID / ID : $id");
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SIZE.screen(context).height,
      width: SIZE.screen(context).width,
      child: Stack(
        children: <Widget>[
          ClipPath(
              clipper: CurveClipper(),
              child: ChildCurveContainer.container(context)),
          chat(),
          Container(
            padding: EdgeInsets.all(10),
            child: Align(
              alignment: Alignment.bottomRight,
              child: FloatingActionButton(
                backgroundColor: COLOR.primary(),
                foregroundColor: Colors.white,
                onPressed: () {
                  return Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ChatAddFromOrderUI()));
                },
                child: Icon(Icons.add_comment),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget chat() {
    return Positioned(
      width: SIZE.screen(context).width,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Container(
          padding: EdgeInsets.only(bottom: 136),
          height: SIZE.screen(context).height,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                    color: Color(0xffeff2f3),
                    offset: Offset(1, 5.0),
                    blurRadius: 3.0)
              ]),
          child: Column(
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(top: 19),
                  child: ListView(
                    physics: BouncingScrollPhysics(),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    children: <Widget>[
                      Text(
                        "Ruang chat",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 19),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      _buildSection("Global"),
                      Divider(height: 5.0),
                      _buildStatusItem(context),
                      _buildSection("Recently"),
                      Container(
                        child: StreamBuilder(
                          stream: chatBLOC.streamChatList,
                          builder: (BuildContext context,
                              AsyncSnapshot<ChatListModel> snapshot) {
                            if (snapshot.hasData) {
                              return listChat(context, snapshot);
                            } else if (snapshot.hasError) {
                              return Center(
                                child: Text(snapshot.error.toString()),
                              );
                            } else {
                              return Container(
                                padding: EdgeInsets.only(top: 80),
                                child: Center(child: Loading.spin(context)),
                              );
                            }
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildStatusItem(context) {
    return GestureDetector(
        onTap: () => Navigator.push(
            context, MaterialPageRoute(builder: (_) => GeneralChatScreen())),
        child: ListTile(
          leading: Stack(
            children: <Widget>[
              Image(
                image: AssetImage('assets/images/general_chat.png'),
              ),
              Container(
                alignment: AlignmentDirectional.bottomEnd,
                width: 42,
                height: 42,
                child: Container(
                  width: 15.0,
                  height: 15.0,
                  child: new RawMaterialButton(
                    onPressed: () {},
                    child: new Icon(
                      Icons.people,
                      color: Colors.white,
                      size: 15.0,
                    ),
                    shape: new CircleBorder(),
                    elevation: 2.0,
                    fillColor: Theme.of(context).accentColor,
                  ),
                ),
              ),
            ],
          ),
          title: Text(
            "General Chat",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: Text(
            "Ruang Chat Global",
            style: TextStyle(color: Colors.grey, fontSize: 14.0),
          ),
        ));
  }

  Widget _buildSection(String name) {
    return Container(
      height: 30.0,
      alignment: AlignmentDirectional.centerStart,
      decoration: BoxDecoration(
        color: Colors.grey[200],
        border: Border(
          top: BorderSide(
            color: Colors.grey,
            width: 0.1,
          ),
        ),
      ),
      child: Padding(
        padding: EdgeInsets.only(left: 15.0),
        child: Text(
          name,
          style: TextStyle(
            fontSize: 15,
          ),
        ),
      ),
    );
  }

  Widget listChat(BuildContext context, AsyncSnapshot<ChatListModel> snapshot) {
    return ListView.builder(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      itemCount: snapshot.data.data.length,
      itemBuilder: (BuildContext context, int index) {
        return snapshot.data.data[index].namaPasien != null
            ? GestureDetector(
                onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (_) => ChatPrivate(
                              idOrder:
                                  snapshot.data.data[index].channel.toString(),
                              customer: snapshot.data.data[index].customer,
                              idOrderString:
                                  snapshot.data.data[index].nama.toString(),
                              pasien: snapshot.data.data[index].namaPasien,
                            ))),
                child: ListTile(
                  leading: new CircleAvatar(
                    foregroundColor: Colors.white,
                    backgroundColor: COLOR.primary(),
                    child: Text(
                      "${snapshot.data.data[index].customer.substring(0, 1)}",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  title: Text(
                      "${snapshot.data.data[index].customer} #${snapshot.data.data[index].nama}",
                      style: new TextStyle(fontWeight: FontWeight.bold)),
                  subtitle:
                      Text("Pasien : ${snapshot.data.data[index].namaPasien}"),
                ),
              )
            : Container();
      },
    );
  }
}
