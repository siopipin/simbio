import 'package:flutter/material.dart';
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/chat/models/chat_model.dart';
import 'package:simbio/src/chat/ui/chat_photo_list.dart';
import 'package:simbio/src/chat/ui/widgets/chat_message.dart';
import 'package:simbio/src/chat/util/chat_database.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/util/toas.dart';
import 'package:toast/toast.dart';

class ChatOnDB extends StatefulWidget {
  final namatbl;
  ChatOnDB({Key key, this.namatbl}) : super(key: key);

  _ChatOnDBState createState() => _ChatOnDBState();
}

class _ChatOnDBState extends State<ChatOnDB> {
  var id;
  List<ChatModel> chatData;
  final List<ChatMessage> _messages = <ChatMessage>[];
  ScrollController _scrollController = new ScrollController();
  final TextEditingController _chatController = new TextEditingController();
  @override
  void initState() {
    super.initState();
    showChatFromServer();
  }

  showChatFromServer() async {
    id = await AuthRepository().hasId();
    chatData = await ChatDBProvider.db.getAllChat(namatbl: widget.namatbl);
    for (int i = chatData.length - 1; i >= 0; i--) {
      ChatMessage messageData = new ChatMessage(
        idTekniker: id,
        text: "${chatData[i].pesan}",
        id: "${chatData[i].pengirim}",
        time: "${chatData[i].date}",
        name: "${chatData[i].nama}",
        tbl: "${chatData[i].tbl}",
        privilege: "${chatData[i].privilege}",
        imgFromdb: "${chatData[i].lampiran}",
      );
      setState(() {
        _messages.add(messageData);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          title: new Text("Chat"),
          backgroundColor: COLOR.background(),
          elevation: 0,
          actions: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ChatOnDB()));
              },
              child: Icon(
                Icons.ac_unit,
                size: 25,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ChatPhotoList()));
              },
              child: Icon(
                Icons.photo_library,
                size: 25,
              ),
            ),
            SizedBox(
              width: 10,
            )
          ],
        ),
        body: Column(
          children: <Widget>[
            new Flexible(
              child: ListView.builder(
                controller: _scrollController,
                padding: new EdgeInsets.all(8.0),
                reverse: true,
                itemBuilder: (_, int index) => _messages[index],
                itemCount: _messages.length,
              ),
            ),
            new Divider(
              height: 1.0,
            ),
            new Container(
              decoration: new BoxDecoration(
                color: Theme.of(context).cardColor,
              ),
              child: _chatEnvironment(),
            ),
          ],
        ));
  }

  Widget _chatEnvironment() {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          border: BorderDirectional(
            top: BorderSide(
              color: Colors.grey,
              width: 0.5,
            ),
          ),
        ),
        child: Align(
          alignment: FractionalOffset.bottomCenter,
          child: Row(
            children: <Widget>[
              Expanded(
                child: TextFormField(
                  controller: _chatController,
                  maxLines: null,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 25),
                    hintText: "Type a message",
                    hintStyle: TextStyle(
                      color: Colors.black38,
                      fontSize: 14,
                    ),
                  ),
                  style: TextStyle(
                    color: Colors.black54,
                    fontSize: 12,
                  ),
                ),
              ),
              IconButton(
                icon: Icon(Icons.image, color: Colors.grey),
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(Icons.photo_camera, color: Colors.grey),
                onPressed: () {},
              ),
              GestureDetector(
                onTap: () {
                  print(
                      "chat_screen / _chatEnv / isi pesan : ${_chatController.text}");
                  if (_chatController.text.length > 0) {
                    // sendNotification(_chatController.text);
                    // sendChatMessage(_chatController.text);
                    // handleSubmitChatWitoutImage(_chatController.text);
                  } else {
                    return showToast("Pesan tidak boleh kosong", context,
                        duration: 2, gravity: Toast.BOTTOM);
                  }
                },
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 8),
                  margin: EdgeInsets.only(right: 20, left: 5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: COLOR.primary(),
                  ),
                  child: Icon(
                    Icons.send,
                    color: Colors.white,
                    size: 18,
                  ),
                ),
              )
            ],
          ),
        ));
  }

  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     appBar: AppBar(
  //       title: Text("DB"),
  //     ),
  //     body: FutureBuilder(
  //       future: ChatDBProvider.db.getAllChat(),
  //       builder:
  //           (BuildContext context, AsyncSnapshot<List<ChatModel>> snapshot) {
  //         if (snapshot.hasData) {
  //           return ListView.builder(
  //             itemCount: snapshot.data.length,
  //             itemBuilder: (BuildContext context, int index) {
  //               return Column(
  //                 children: <Widget>[
  //                   Container(
  //                     padding: EdgeInsets.all(10),
  //                     child: RaisedButton(
  //                       onPressed: () {
  //                         ChatDBProvider.db.deleteAllChat();
  //                       },
  //                       child: Text("Hapus"),
  //                     ),
  //                   ),
  //                   Container(
  //                     child: ListTile(
  //                       title: Text('${snapshot.data[index].pengirim}'),
  //                       trailing: Text('${snapshot.data[index].pesan}'),
  //                     ),
  //                   )
  //                 ],
  //               );
  //             },
  //           );
  //         } else if (snapshot.hasError) {
  //           return Center(
  //             child: Text(snapshot.error.toString()),
  //           );
  //         } else {
  //           return Center(
  //             child: CircularProgressIndicator(),
  //           );
  //         }
  //       },
  //     ),
  //   );
  // }
}
