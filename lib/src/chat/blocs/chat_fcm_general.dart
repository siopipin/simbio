import 'dart:convert';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' show Client, Response;
import 'package:simbio/src/util/pustaka.dart';

class ChatFCMGeneral {
  Client client = Client();
  FirebaseMessaging _firebaseMessaging;
  String globalFCMToken;
  String idclient;

  void setUpFirebase(BuildContext context) {
    _firebaseMessaging = FirebaseMessaging();
    firebaseCloudMessagingListeners(context);
  }

  void firebaseCloudMessagingListeners(BuildContext context) {
    if (Platform.isIOS) iOSPermission();
    _firebaseMessaging.subscribeToTopic('general');

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print(
            'Chat_Fcm_General / firebaseCloudMessagingListeners [GLOBAL] / message : $message');
      },
      onResume: (Map<String, dynamic> message) async {
        print(
            'Chat_Fcm_General / firebaseCloudMessagingListeners [onResume] / message : $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print(
            'Chat_Fcm_General / firebaseCloudMessagingListeners [onLaunch] / message : $message');
      },
    );
  }

  void iOSPermission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  Future<void> sendNotificationChat(
      {@required String topic,
      @required String pengirim,
      @required String pesan}) async {
    Response response;
    final String url = '${URL.api()}notifikasi/send-notifikasi/';
    final body = json.encode({
      "to": topic,
      "title": pengirim,
      "body": pesan,
    });
    response = await client.post(url, body: body);
    if (response.statusCode == 200) {
      return print('success');
    } else {
      throw Exception('Failed to load order');
    }
  }
}
