import 'dart:async';

import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:simbio/src/chat/models/chat_list_model.dart';
import 'package:simbio/src/chat/models/chat_photo_list_model.dart';
import 'package:simbio/src/chat/models/message_model.dart';
import 'package:simbio/src/chat/resources/chat_repository.dart';
import 'package:simbio/src/order/models/orders_model.dart';

class ChatBloc {
  ChatRepository chatRepository = ChatRepository();

  final chatMessageData = PublishSubject<MessageModel>();
  Observable<MessageModel> get streamChat => chatMessageData.stream;

  final orderData = PublishSubject<OrdersModel>();
  Observable<OrdersModel> get streamOrder => orderData.stream;

  final chatListData = PublishSubject<ChatListModel>();
  Observable<ChatListModel> get streamChatList => chatListData.stream;

  final chatPrivateData = PublishSubject<MessageModel>();
  Observable<MessageModel> get streamChatPrivate => chatPrivateData.stream;

  final chatPhotoGeneral = PublishSubject<ChatPhotosListModel>();
  Observable<ChatPhotosListModel> get streamChatPhotoGeneral =>
      chatPhotoGeneral.stream;

  final chatPhotoPrivate = PublishSubject<ChatPhotosListModel>();
  Observable<ChatPhotosListModel> get streamchatPhotoPrivate =>
      chatPhotoPrivate.stream;

  Future<MessageModel> doGetChat(
      {@required int start, @required int limit}) async {
    MessageModel itemModel =
        await chatRepository.getChatMessage(start: start, limit: limit);
    chatMessageData.sink.add(itemModel);
    print('chat_bloc / doGetChat / itemModel : $itemModel');
    return itemModel;
  }

  Future<ChatListModel> doGetChatList({@required int id}) async {
    ChatListModel itemModel = await chatRepository.getChatList(id: id);
    chatListData.sink.add(itemModel);
    return itemModel;
  }

  Future<MessageModel> doGetChatPrivate(
      {@required int channel, @required int start, @required int limit}) async {
    MessageModel itemModel = await chatRepository.getChatPrivate(
        channel: channel, start: start, limit: limit);
    chatPrivateData.sink.add(itemModel);
    return itemModel;
  }

  Future<ChatPhotosListModel> getPhotoListPrivate(
      {@required int channel, @required int start, @required int limit}) async {
    ChatPhotosListModel itemModel = await chatRepository.getPhotoListPrivate(
        channel: channel, start: start, limit: limit);
    chatPhotoPrivate.sink.add(itemModel);
    return itemModel;
  }

  Future<ChatPhotosListModel> getPhotoListGeneral(
      {@required int start, @required int limit}) async {
    ChatPhotosListModel itemModel =
        await chatRepository.getPhotoListGeneral(start: start, limit: limit);
    chatPhotoGeneral.sink.add(itemModel);
    return itemModel;
  }

  doGetOrder() async {
    OrdersModel itemModel = await chatRepository.getOrder();
    orderData.sink.add(itemModel);
  }

  dispose() {
    chatMessageData.close();
    orderData.close();
    chatListData.close();
    chatPrivateData.close();
    chatPhotoGeneral.close();
    chatPhotoPrivate.close();
  }
}

final chatBLOC = ChatBloc();
