import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:simbio/src/order/models/tekniker_order_model.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/util/response_model.dart';
import 'package:simbio/src/order/blocs/order_bloc.dart';
import 'package:simbio/src/order/models/progress_model.dart';
import 'package:meta/meta.dart';
import 'package:async/async.dart';
import 'package:simbio/src/util/toas.dart';
import 'package:toast/toast.dart';

onLoading(
    {@required BuildContext context,
    @required idorder,
    @required idprodct,
    @required ProgressModel data,
    @required int currentstep,
    @required int idpekerja,
    @required int nexstatus,
    @required String date}) {
  print(
      'Currenstep: $currentstep | idpekerja : $idpekerja | nexstatus: $nexstatus | date: $date');
  showDialog(
    context: context,
    barrierDismissible: false,
    child: Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    ),
  );
  new Future.delayed(Duration.zero, () async {
    print(data.data[currentstep].id);
    await ordersBLOC.doUpdateProgress(
        idorder: data.data[currentstep].id,
        idproduk: data.data[currentstep].idProduct,
        idtekniker: idpekerja,
        status: nexstatus,
        tanggalselesai: date.toString(),
        idprogress: data.data[currentstep].idprogress);
    await ordersBLOC.doGetProgress(idorder: idorder, idproduct: idprodct);
    Navigator.pop(context);
  });
}

//ON POST QUALITY CONTROL
doUpdateQualityControl(
    {@required BuildContext context,
    @required int anatomi,
    @required int idorder,
    @required int idproduct,
    @required int margin,
    @required int oksilus,
    @required int pic,
    @required int warna,
    @required String tanggal}) {
  showDialog(
    context: context,
    barrierDismissible: false,
    child: Container(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    ),
  );
  new Future.delayed(Duration.zero, () async {
    final storage = new FlutterSecureStorage();
    await ordersBLOC.doUpdateProduct(idorder: idorder.toString(), idproduct: idproduct.toString());
    ResponseModel res = await ordersBLOC.doPostQualityControl(
        tanggal: tanggal,
        warna: warna,
        pic: pic,
        oksilus: oksilus,
        margin: margin,
        idorder: idorder,
        idproduk: idproduct,
        anatomi: anatomi);
    if (res.statuscode == 200) {
      print("di status code 200");
      await storage.write(key: 'done', value: "false");
      showToast(res.message, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      Navigator.of(context).pop();
      // Navigator.of(context).pop();
    } else {
      await storage.write(key: 'done', value: "true");
      showToast(res.message, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      Navigator.of(context).pop();
      // Navigator.of(context).pop();
    }
  });
}

doNotifSPV({@required String idorder}) async {
  PegawaiinOrderModel pegawaiinOrderModel =
      await ordersBLOC.doGetTeknikerinOrder(idorder: idorder);
  if (pegawaiinOrderModel.status == 200) {
    //TODO: Perbaiki docekSPV
    String tokenFCM;
    for (int i = 0; i < pegawaiinOrderModel.data.length; i++) {
      if (pegawaiinOrderModel.data[i].privilege == 3) {
        print("PRIVILEGE 3 FOUND");
        tokenFCM = pegawaiinOrderModel.data[i].fcmtoken;
      } else {
        tokenFCM = null;
      }
    }
    print('INI TOKEN FCM SPV $tokenFCM');
    await ordersBLOC.doSendNotiftoSPV(token: tokenFCM, idorder: idorder);
  } else {
    pegawaiinOrderModel = null;
  }
}

doCekSPV(PegawaiinOrderModel data) async {
  for (int i = 0; i < data.data.length; i++) {
    if (data.data[i].privilege == 3) {
      print('TOKEN SUPERVISOR ${data.data[i].fcmtoken}');
      return data.data[i].fcmtoken;
    } else {
      return null;
    }
  }
}

doFormatTanggal(String tanggal) {
  var date = DateTime.parse(tanggal);
  var formatter = new DateFormat('yyyy-MM-dd');
  String res = formatter.format(date);
  return res;
}

doCekStatus(String status) {
  if (status == "1") {
    return "On Progress";
  } else {
    return "Selesai";
  }
}
