import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:http/http.dart';
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/order/models/order_detail_model.dart';
import 'package:http/http.dart' show Client;
import 'package:simbio/src/order/models/order_detail_scan.dart';
import 'package:simbio/src/order/models/progress_model.dart';

class OrderBloc {
  Client client = Client();
  Dio dio = new Dio();

  Future<OrderDetailModel> getOrderDetail({String id}) async {
    try {
      final token = await AuthRepository().hasToken();
      final url = '${URL.api()}tekniker/order/$id';
      final res = await client
          .get(url, headers: {HttpHeaders.authorizationHeader: 'Barer $token'});
      final data = json.decode(res.body);
      data['statusCode'] = res.statusCode;

      if (res.statusCode == 200) {
        final result = OrderDetailModel.fromJson(data);
        return result;
      }
      if (res.statusCode == 400) {
        final result = OrderDetailModel.fromJson(data);
        throw Exception(result.msg.toString());
      }
      if (res.statusCode == 401) {
        final result = OrderDetailModel.fromJson(data);
        throw Exception(result.msg.toString());
      } else {
        final dataElse = json.decode(res.body);
        dataElse['statusCode'] = res.statusCode;
        dataElse['msg'] = 'Server Message Notification';
        final result = OrderDetailModel.fromJson(dataElse);
        return result;
      }
    } on Exception {
      rethrow;
    }
  }

  Future<ProgressModel> doGetProgress(
      {String idOrder, String idProduct}) async {
    try {
      final token = await AuthRepository().hasToken();
      final url = '${URL.api()}tekniker/progress/$idOrder/$idProduct';
      final res = await client
          .get(url, headers: {HttpHeaders.authorizationHeader: 'Barer $token'});
      final data = json.decode(res.body);
      data['statusCode'] = res.statusCode;

      if (res.statusCode == 200) {
        final result = ProgressModel.fromJson(data);
        return result;
      }
      if (res.statusCode == 400) {
        final result = ProgressModel.fromJson(data);
        throw Exception(result.msg.toString());
      }
      if (res.statusCode == 401) {
        throw Exception(data['message']);
      } else {
        throw Exception('Error when fetching');
      }
    } on Exception {
      rethrow;
    }
  }

  Future<DetailOrderScanModel> doGetOrder({String idOrder}) async {
    try {
      final token = await AuthRepository().hasToken();
      final url = '${URL.api()}tekniker/progress-order/$idOrder';
      final res = await client
          .get(url, headers: {HttpHeaders.authorizationHeader: 'Barer $token'});
      final data = json.decode(res.body);
      data['statusCode'] = res.statusCode;
      if (res.statusCode == 200) {
        final result = DetailOrderScanModel.fromJson(data);
        return result;
      }
      if (res.statusCode == 400) {
        throw Exception(res.statusCode);
      }
      if (res.statusCode == 401) {
        throw Exception(data['message']);
      } else {
        throw Exception('Error when fetching');
      }
    } on Exception {
      rethrow;
    }
  }

  doGetOrderScan({String idOrder}) async {
    var token = await AuthRepository().hasToken();
    dio.options.connectTimeout = 25000;
    dio.options.receiveTimeout = 25000;

    dio.options.headers = {HttpHeaders.authorizationHeader: 'Barer $token'};

    final url = '${URL.api()}tekniker/progress-order/$idOrder';
    final response = await dio.get(url);
    return response;
  }

  doUpdateProgress({int idorder, int idproduk, int idtekniker, int status, String tanggalselesai, int idprogress}) async {

    try {
      final token = await AuthRepository().hasToken();
      final url = '${URL.api()}tekniker/update-progress/';
      final bodyNotset = {
        "id_order": idorder,
        "id_product": idproduk,
        "id_progress": idprogress,
        "status": status,
        "tanggal_selesai": tanggalselesai,
        "id_tekniker": idtekniker
      };
      final body = json.encode(bodyNotset);
      final res = await client.put(url,
          headers: {
            HttpHeaders.authorizationHeader: 'Barer $token',
            'Content-type': 'application/json',
            'Accept': 'application/json'
          },
          body: body);
    } catch (e) {
      print(e.toString());
    }
  }
}

final orderBLOC = OrderBloc();
