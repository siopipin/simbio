import 'dart:convert';
import 'dart:io';

import 'package:rxdart/rxdart.dart';
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/order/models/orders_model.dart';
import 'package:simbio/src/order/models/orders_riwayat_model.dart';
import 'package:simbio/src/order/models/status_quality_model.dart';
import 'package:simbio/src/order/models/tekniker_order_model.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/util/response_model.dart';
import 'package:simbio/src/order/models/order_detail_model.dart';
import 'package:http/http.dart' show Client;
import 'package:simbio/src/order/models/progress_model.dart';
import 'package:simbio/src/order/repository/order_repo.dart';
import 'package:meta/meta.dart';

class OrderBloc {
  final OrderRepo orderRepo = OrderRepo();
  Client client = Client();

  final BehaviorSubject<OrdersModel> dataOrder = BehaviorSubject<OrdersModel>();

  final BehaviorSubject<OrdersRiwayatModel> dataOrderHistory =
      BehaviorSubject<OrdersRiwayatModel>();

  final BehaviorSubject<ProgressModel> dataProgress =
      BehaviorSubject<ProgressModel>();

  final BehaviorSubject<OrderDetailModel> dataDetailOrder =
      BehaviorSubject<OrderDetailModel>();

  final BehaviorSubject<PegawaiinOrderModel> dataPegawaiInOrder =
      BehaviorSubject<PegawaiinOrderModel>();

  //Event
  doGetOrder() async {
    OrdersModel ordersModel = await orderRepo.getOrder();
    dataOrder.sink.add(ordersModel);
    return ordersModel;
  }

  doGetOrderHistory() async {
    OrdersRiwayatModel ordersModel = await orderRepo.getOrderHistory();
    dataOrderHistory.sink.add(ordersModel);
  }

  doGetOrderDetail({@required String idOrder}) async {
    OrderDetailModel orderDetailModel =
        await orderRepo.getOrderDetail(idOrder: idOrder);
    dataDetailOrder.sink.add(orderDetailModel);
    return orderDetailModel;
  }

  doGetProgress({String idorder, String idproduct}) async {
    ProgressModel progressModel =
        await orderRepo.getProgress(idOrder: idorder, idProduct: idproduct);
    dataProgress.sink.add(progressModel);
    return progressModel;
  }

  doUpdateProgress(
      {@required int idorder,
      @required int idproduk,
      @required int idtekniker,
      @required int status,
      @required String tanggalselesai,
      @required int idprogress}) async {
    ResponseModel res = await orderRepo.updateProgress(
        idorder: idorder,
        idproduk: idproduk,
        idtekniker: idtekniker,
        status: status,
        tanggalselesai: tanggalselesai,
        idprogress: idprogress);
    return res;
  }

  doPostQualityControl(
      {@required int idorder,
      @required int idproduk,
      @required int margin,
      @required int oksilus,
      @required int anatomi,
      @required int warna,
      @required int pic,
      @required String tanggal}) async {
    print("ini pic bloc $pic");
    ResponseModel res = await orderRepo.postQualityControl(
        idorder: idorder,
        idproduk: idproduk,
        margin: margin,
        oksilus: oksilus,
        anatomi: anatomi,
        warna: warna,
        pic: pic,
        tanggal: tanggal);
    return res;
  }

  doGetTeknikerinOrder({@required String idorder}) async {
    PegawaiinOrderModel responseModel =
        await orderRepo.getTeknikerinOrder(idorder: idorder);
    dataPegawaiInOrder.sink.add(responseModel);
    return responseModel;
  }

  Future<ResponseModel> doSendNotiftoSPV(
      {@required token, @required idorder}) async {
    Client client = Client();
    final authFCM = '${URL.keyAuthFCM()}';
    final url = 'https://fcm.googleapis.com/fcm/send';
    final body = json.encode({
      "to": token,
      "notification": {
        "body": "Order $idorder Memerlukan Approve Anda!",
        "title": "Order dengan ID $idorder Memerlukan Approve !!",
        "content_available": true,
        "priority": "high"
      }
    });

    print(body);
    final res = await client.post(url,
        headers: {
          HttpHeaders.authorizationHeader: '$authFCM',
          'Content-type': 'application/json',
          'Accept': 'application/json'
        },
        body: body);

    print(res.statusCode);
    print(res.body);

    final data = json.decode(res.body);
    data['statusCode'] = res.statusCode;
    if (res.statusCode == 200) {
      data['msg'] = "Successfully";
      final result = ResponseModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 400) {
      data['msg'] = "Cannot get data from server";
      final result = ResponseModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 401) {
      data['msg'] = "Not authorized or invalid token";
      final result = ResponseModel.fromJson(data);
      return result;
    } else {
      data['msg'] = "Error from server";
      final result = ResponseModel.fromJson(data);
      return result;
    }
  }

  Future<StatusQualityControlModel> doCekQuality(
      {@required String idorder, @required String idproduct}) async {
    final token = await AuthRepository().hasToken();
    final url = '${URL.api()}tekniker/cek-quality/$idorder/$idproduct';
    final res = await client.get(url, headers: {
      HttpHeaders.authorizationHeader: 'Barer $token',
      'Content-type': 'application/json',
      'Accept': 'application/json'
    });
    final data = json.decode(res.body);
    data['statuscode'] = res.statusCode;
    if (res.statusCode == 200) {
      data['message'] = "Successfully";
      final result = StatusQualityControlModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 400) {
      data['message'] = "Cannot get data from server";
      final result = StatusQualityControlModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 401) {
      data['message'] = "Not authorized or invalid token";
      final result = StatusQualityControlModel.fromJson(data);
      return result;
    } else {
      data['message'] = "Error from server";
      final result = StatusQualityControlModel.fromJson(data);
      return result;
    }
  }

  Future<ResponseModel> doUpdateProduct(
      {@required String idorder, @required String idproduct}) async {
    final token = await AuthRepository().hasToken();
    final url = '${URL.api()}tekniker/update-produk-selesai/';
    final body = json.encode({
      "id_order": idorder,
      "id_product": idproduct,
    });
    final res = await client.put(url,
        headers: {
          HttpHeaders.authorizationHeader: 'Barer $token',
          'Content-type': 'application/json',
          'Accept': 'application/json'
        },
        body: body);
    final data = json.decode(res.body);
    data['statuscode'] = res.statusCode;
    if (res.statusCode == 200) {
      data['message'] = "Successfully";
      final result = ResponseModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 400) {
      data['message'] = "Cannot get data from server";
      final result = ResponseModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 401) {
      data['message'] = "Not authorized or invalid token";
      final result = ResponseModel.fromJson(data);
      return result;
    } else {
      data['message'] = "Error from server";
      final result = ResponseModel.fromJson(data);
      return result;
    }
  }

  dispose() {
    dataOrder.close();
    dataProgress.close();
    dataDetailOrder.close();
    dataPegawaiInOrder.close();
    dataOrderHistory.close();
  }

  //Stream / Keluaran
  BehaviorSubject<OrdersModel> get streamDataOrder => dataOrder;
  BehaviorSubject<OrdersRiwayatModel> get streamDataOrderHistory =>
      dataOrderHistory;
  BehaviorSubject<ProgressModel> get streamDataProgress => dataProgress;
  BehaviorSubject<OrderDetailModel> get streamDataDetailOrder =>
      dataDetailOrder;
  BehaviorSubject<PegawaiinOrderModel> get streaDataPegawaiInOrder =>
      dataPegawaiInOrder;
}

final ordersBLOC = OrderBloc();
