import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class OrdersEvent extends Equatable {
  OrdersEvent([List props = const []]) : super(props);
}

class Fetch extends OrdersEvent {
  @override
  String toString() {
    return "OrderEvent.Fetch";
  }
}