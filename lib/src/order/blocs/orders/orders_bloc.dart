import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:simbio/src/auth/bloc/authentication/bloc.dart';
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/order/models/orders_model.dart';
import 'package:simbio/src/order/repository/order_repository.dart';
import './bloc.dart';

class OrdersBloc extends Bloc<OrdersEvent, OrdersState> {
  @override
  OrdersState get initialState => InitialOrdersState();

  @override
  Stream<OrdersState> mapEventToState(
    OrdersEvent event,
  ) async* {
     final orderRepository = OrderRepository();
    final authrepo = AuthRepository();
    final authbloc = AuthenticationBloc(authRepository: authrepo);
    if(event is Fetch) {
      yield InitialOrdersState();
      try {
        final response = await orderRepository.getOrders();
        if(response.statusCode == 401) {
          authbloc.dispatch(LoggedOut());
          yield OrdersUnAuth();
        } else if (response.statusCode == 400) {
          final result = OrdersModel.fromJson(json.decode(response.body)) ;
          yield OrdersError(error: "Message: ${result.message}");
          throw Exception("${result.message}");
        } else if (response.statusCode == 200) {
          final ordersModel = OrdersModel.fromJson(json.decode(response.body));
          yield OrdersLoaded(ordersModel: ordersModel);
        } else {
          yield OrdersError(error: "statusCode");
        }
      } catch (e) {
        yield OrdersError(error: e.toString());
      }
    }
  }
}
