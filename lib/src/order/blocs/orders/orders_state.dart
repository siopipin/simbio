import 'package:equatable/equatable.dart';
import 'package:simbio/src/order/models/orders_model.dart';

abstract class OrdersState extends Equatable {
  OrdersState([List props = const []]) : super(props);
}

class InitialOrdersState extends OrdersState {
  @override
  String toString() {
    return "BankState.InitialbankState";
  }
}

class OrdersError extends OrdersState {
  final String error;
  OrdersError({this.error}) : super([error]);

  @override
  String toString() {
    return "OrdersState.OrdersError.message : $error";
  }
}

class OrdersUnAuth extends OrdersState {
  @override
  String toString() {
    return 'OrdersState.OrdersUnAuth';
  }
}

class OrdersLoaded extends OrdersState {
  OrdersModel ordersModel;
  OrdersLoaded({this.ordersModel}) {
    this.ordersModel = ordersModel;
  }
  @override
  String toString() {
    return "OrdersState.OrdersLoaded";
  }
}
