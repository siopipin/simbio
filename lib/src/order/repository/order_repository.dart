import 'dart:async';
import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' show Client;
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/util/pustaka.dart';

class OrderRepository {
  final String url = '${URL.api()}tekniker/orders';
  final storage = new FlutterSecureStorage();
  Client client = new Client();

  Future getOrders() async {
    final token = await AuthRepository().hasToken();
    try {
      final response = await client
          .get(url, headers: {HttpHeaders.authorizationHeader : 'Bearer $token'});
        print(response.statusCode);
      return response;
    } on Exception {
      rethrow;
    }
  }
}
