import 'dart:async';
import 'package:meta/meta.dart';
import 'package:simbio/src/order/models/orders_model.dart';
import 'package:simbio/src/order/models/orders_riwayat_model.dart';
import 'package:simbio/src/order/models/tekniker_order_model.dart';
import 'package:simbio/src/util/response_model.dart';
import 'package:simbio/src/order/models/order_detail_model.dart';
import 'package:simbio/src/order/models/progress_model.dart';
import 'package:simbio/src/order/repository/order_prov.dart';

class OrderRepo {
  final OrderProv orderProv = OrderProv();

  Future<OrdersModel> getOrder() => orderProv.getOrder();

  Future<OrdersRiwayatModel> getOrderHistory() => orderProv.getOrderHistory();

  Future<OrderDetailModel> getOrderDetail({@required String idOrder}) =>
      orderProv.getOrderDetail(id: idOrder);

  Future<ProgressModel> getProgress({String idOrder, String idProduct}) =>
      orderProv.getProgress(idOrder: idOrder, idProduct: idProduct);

  Future<ResponseModel> updateProgress(
          {@required int idorder,
          @required int idproduk,
          @required int idtekniker,
          @required int status,
          @required String tanggalselesai,
          @required int idprogress}) =>
      orderProv.updateProgress(
          idorder: idorder,
          idproduk: idproduk,
          idtekniker: idtekniker,
          status: status,
          tanggalselesai: tanggalselesai,
          idprogress: idprogress);

  Future<ResponseModel> postQualityControl(
          {@required int idorder,
          @required int idproduk,
          @required int margin,
          @required int oksilus,
          @required int anatomi,
          @required int warna,
          @required int pic,
          @required String tanggal}) =>
      orderProv.postQualityControl(
          idorder: idorder,
          idproduk: idproduk,
          margin: margin,
          oksilus: oksilus,
          anatomi: anatomi,
          warna: warna,
          pic: pic,
          tanggal: tanggal);

  Future<PegawaiinOrderModel> getTeknikerinOrder({@required String idorder}) =>
      orderProv.getTeknikerinOrder(idorder: idorder);
}
