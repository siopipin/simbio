import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' show Client;
import 'package:http/http.dart';
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/order/models/orders_model.dart';
import 'package:simbio/src/order/models/orders_riwayat_model.dart';
import 'package:simbio/src/order/models/tekniker_order_model.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/util/response_model.dart';
import 'package:simbio/src/order/models/order_detail_model.dart';
import 'package:simbio/src/order/models/progress_model.dart';
import 'package:meta/meta.dart';

class OrderProv {
  Client client = Client();

  Future<OrdersModel> getOrder() async {
    final token = await AuthRepository().hasToken();
    final url = '${URL.api()}tekniker/orders';
    final res = await client
        .get(url, headers: {HttpHeaders.authorizationHeader: 'Barer $token'});

    print(res.statusCode);
    print(res.body);
    final data = json.decode(res.body);
    data['statuscode'] = res.statusCode;
    if (res.statusCode == 200) {
      data['message'] = "Successfully";
      final result = OrdersModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 400) {
      data['message'] = "Cannot get data from server";
      final result = OrdersModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 401) {
      data['message'] = "Not authorized or invalid token";
      final result = OrdersModel.fromJson(data);
      return result;
    } else {
      data['message'] = "Error from server";
      final result = OrdersModel.fromJson(data);
      return result;
    }
  }

   Future<OrdersRiwayatModel> getOrderHistory() async {
    final token = await AuthRepository().hasToken();
    final url = '${URL.api()}tekniker/orders-riwayat';
    final res = await client
        .get(url, headers: {HttpHeaders.authorizationHeader: 'Barer $token'});

    print(res.statusCode);
    print(res.body);
    final data = json.decode(res.body);
    data['statuscode'] = res.statusCode;
    if (res.statusCode == 200) {
      data['message'] = "Successfully";
      final result = OrdersRiwayatModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 400) {
      data['message'] = "Cannot get data from server";
      final result = OrdersRiwayatModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 401) {
      data['message'] = "Not authorized or invalid token";
      final result = OrdersRiwayatModel.fromJson(data);
      return result;
    } else {
      data['message'] = "Error from server";
      final result = OrdersRiwayatModel.fromJson(data);
      return result;
    }
  }

  Future<OrderDetailModel> getOrderDetail({@required String id}) async {
    final token = await AuthRepository().hasToken();
    final url = '${URL.api()}tekniker/order/$id';
    final res = await client
        .get(url, headers: {HttpHeaders.authorizationHeader: 'Barer $token'});

    final data = json.decode(res.body);
    data['statusCode'] = res.statusCode;
    if (res.statusCode == 200) {
      data['msg'] = "Successfully";
      final result = OrderDetailModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 400) {
      data['msg'] = "Cannot get data from server";
      final result = OrderDetailModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 401) {
      data['msg'] = "Not authorized or invalid token";
      final result = OrderDetailModel.fromJson(data);
      return result;
    } else {
      data['msg'] = "Error from server";
      final result = OrderDetailModel.fromJson(data);
      return result;
    }
  }

  Future<ProgressModel> getProgress({String idOrder, String idProduct}) async {
    final token = await AuthRepository().hasToken();
    final url = '${URL.api()}tekniker/progress/$idOrder/$idProduct';
    final res = await client
        .get(url, headers: {HttpHeaders.authorizationHeader: 'Barer $token'});
    final data = json.decode(res.body);
    data['statusCode'] = res.statusCode;

    if (res.statusCode == 200) {
      final result = ProgressModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 400) {
      final result = ProgressModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 401) {
      final result = ProgressModel.fromJson(data);
      return result;
    } else {
      final result = ProgressModel.fromJson(data);
      return result;
    }
  }

  Future<ResponseModel> updateProgress(
      {@required int idorder,
      @required int idproduk,
      @required int idtekniker,
      @required int status,
      @required String tanggalselesai,
      @required int idprogress}) async {
    final token = await AuthRepository().hasToken();
    final url = '${URL.api()}tekniker/update-progress/';
    final bodyNotset = {
      "id_order": idorder,
      "id_product": idproduk,
      "id_progress": idprogress,
      "status": status,
      "tanggal_selesai": tanggalselesai,
      "id_tekniker": idtekniker
    };
    final body = json.encode(bodyNotset);
    print(body);
    final res = await client.put(url,
        headers: {
          HttpHeaders.authorizationHeader: 'Barer $token',
          'Content-type': 'application/json',
          'Accept': 'application/json'
        },
        body: body);

    print(res.statusCode);
    print(res.body);

    final data = json.decode(res.body);
    data['statuscode'] = res.statusCode;
    if (res.statusCode == 200) {
      data['message'] = "Successfully";
      final result = ResponseModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 400) {
      data['message'] = "Cannot get data from server";
      final result = ResponseModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 401) {
      data['message'] = "Not authorized or invalid token";
      final result = ResponseModel.fromJson(data);
      return result;
    } else {
      data['message'] = "Error from server";
      final result = ResponseModel.fromJson(data);
      return result;
    }
  }

  Future<ResponseModel> postQualityControl(
      {@required int idorder,
      @required int idproduk,
      @required int margin,
      @required int oksilus,
      @required int anatomi,
      @required int warna,
      @required int pic,
      @required String tanggal}) async {
    final token = await AuthRepository().hasToken();
    final url = '${URL.api()}tekniker/quality-control/';
    final bodyNotset = {
      "id_order": idorder,
      "id_product": idproduk,
      "margin": margin,
      "oklusi": oksilus,
      "anatomi": anatomi,
      "warna": warna,
      "pic": pic,
      "tanggal": tanggal
    };
    final body = json.encode(bodyNotset);
    print(body);
    final res = await client.post(url,
        headers: {
          HttpHeaders.authorizationHeader: 'Barer $token',
          'Content-type': 'application/json',
          'Accept': 'application/json'
        },
        body: body);
    print(res.statusCode);
    print(res.body);
    final data = json.decode(res.body);
    data['statuscode'] = res.statusCode;
    if (res.statusCode == 200) {
      data['message'] = "Successfully";
      final result = ResponseModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 400) {
      data['message'] = "Cannot get data from server";
      final result = ResponseModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 401) {
      data['message'] = "Not authorized or invalid token";
      final result = ResponseModel.fromJson(data);
      return result;
    } else {
      data['message'] = "Error from server";
      final result = ResponseModel.fromJson(data);
      return result;
    }
  }

  Future<PegawaiinOrderModel> getTeknikerinOrder(
      {@required String idorder}) async {
    final token = await AuthRepository().hasToken();
    final url = '${URL.api()}tekniker/teknikerinorder/$idorder';
    final res = await client.get(url, headers: {
      HttpHeaders.authorizationHeader: 'Barer $token',
      'Content-type': 'application/json',
      'Accept': 'application/json'
    });
    print(res.statusCode);
    print(res.body);
    final data = json.decode(res.body);
    data['statuscode'] = res.statusCode;
    if (res.statusCode == 200) {
      data['message'] = "Successfully";
      final result = PegawaiinOrderModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 400) {
      data['message'] = "Cannot get data from server";
      final result = PegawaiinOrderModel.fromJson(data);
      return result;
    }
    if (res.statusCode == 401) {
      data['message'] = "Not authorized or invalid token";
      final result = PegawaiinOrderModel.fromJson(data);
      return result;
    } else {
      data['message'] = "Error from server";
      final result = PegawaiinOrderModel.fromJson(data);
      return result;
    }
  }
}
