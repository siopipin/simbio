import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simbio/src/util/firebase_notification_handler.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/widgets/curver_clipper_widget.dart';
import 'package:simbio/src/order/blocs/orders/bloc.dart';
import 'package:simbio/src/order/models/orders_model.dart';

//Format tanggal
import 'package:intl/intl.dart';
import 'package:simbio/src/order/screens/detail_produkorder.dart';
import 'package:simbio/src/order/screens/product_progress.dart';

class OrderScreen extends StatefulWidget {
  OrderScreen({Key key}) : super(key: key);

  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  @override
  void initState() {
    super.initState();
    new FirebaseNotifications().setUpFirebase(context);
  }

  String idOrder;
  OrdersModel ordersModel;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: SIZE.screen(context).height,
      width: SIZE.screen(context).width,
      padding: EdgeInsets.only(bottom: 10),
      child: Stack(
        children: <Widget>[
          ClipPath(
              clipper: CurveClipper(),
              child: ChildCurveContainer.container(context)),
          Positioned(
            width: SIZE.screen(context).width,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Container(
                padding: EdgeInsets.only(bottom: 156),
                height: SIZE.screen(context).height,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0xffeff2f3),
                          offset: Offset(1, 5.0),
                          blurRadius: 3.0)
                    ]),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                        child: BlocProvider(
                            builder: (context) =>
                                OrdersBloc()..dispatch(Fetch()),
                            child: orderBody(context)),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget orderBody(BuildContext context) {
    return BlocBuilder<OrdersBloc, OrdersState>(
      builder: (context, state) {
        if (state is OrdersUnAuth) {}
        if (state is InitialOrdersState) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is OrdersError) {
          return Center(
            child: Text("Failed to fetch post"),
          );
        }
        if (state is OrdersLoaded) {
          return listOrder(context, state.ordersModel);
        } else {
          return Center(
            child: Text("Failed to fetch post"),
          );
        }
      },
    );
  }

  Widget listOrder(BuildContext context, OrdersModel data) {
    ordersModel = data;
    return data.orders.length == 0
        ? Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.dvr,
                  color: COLOR.primary(),
                  size: 70,
                ),
                Text("Order tidak ditemukan")
              ],
            ),
          )
        : ListView.builder(
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            itemCount: data.orders.length,
            itemBuilder: (BuildContext context, int index) {
              if (data.orders[index].status != 2) {
                return Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 3, right: 3, bottom: 3),
                      color: (COLOR.secondary()),
                      child: ExpansionTile(
                        title: headerListOrder(context, data.orders[index]),
                        children: <Widget>[
                          data.orders[index].products.length == 0
                              ? Container(
                                  padding: EdgeInsets.only(bottom: 20),
                                  child: Column(
                                    children: <Widget>[
                                      Icon(
                                        Icons.dvr,
                                        color: COLOR.ketiga(),
                                        size: 70,
                                      ),
                                      Text(
                                        "Produk tidak ditemukan",
                                        style: TextStyle(color: Colors.white),
                                      )
                                    ],
                                  ),
                                )
                              : listdataProduct(
                                  context,
                                  data.orders[index].products,
                                  data.orders[index].id.toString()),
                        ],
                      ),
                    ),
                    Container(
                      height: 5,
                      color: Colors.white,
                    )
                  ],
                );
              } else {
                return Container();
              }
            },
          );
  }

  Widget headerListOrder(BuildContext context, datas) {
    Orders data = datas;
    idOrder = data.id.toString();
    //Format Tanggal
    var date = DateTime.parse(data.tanggalTargetSelesai);
    var formatter = new DateFormat('yyyy-MM-dd');
    String tanggalSelesai = formatter.format(date);

    var date2 = DateTime.parse(data.tanggal);
    var formatter2 = new DateFormat('yyyy-MM-dd');
    String tanggalMulai = formatter2.format(date2);

    return GestureDetector(
        onTap: () => Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => DetailProdukOrderScreen(
                  id: data.id.toString(),
                ))),
        child: new Container(
          height: 140.0,
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          alignment: Alignment.centerLeft,
          child: Column(children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(1, 10, 1, 5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    height: 50,
                    width: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.blue[300].withOpacity(0.16)),
                    child: Icon(
                      Icons.check_box,
                      color: Colors.white,
                      size: 28,
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'ORDER #${data.idOrder}',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 19,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        SizedBox(height: 5),
                        Text(
                          data.namaPasien,
                          style: TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Visibility(
                child: Expanded(
                    child: Padding(
              padding: const EdgeInsets.fromLTRB(2, 0, 2, 16),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.only(
                          top: 10,
                        ),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Tanggal Mulai",
                                      style: TextStyle(
                                        color: Colors.white70,
                                        fontSize: 14,
                                      ),
                                    ),
                                    SizedBox(height: 5),
                                    Text(
                                      tanggalMulai,
                                      style: TextStyle(
                                          color: Colors.white70,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Tanggal Selesai",
                                      style: TextStyle(
                                        color: Colors.white70,
                                        fontSize: 14,
                                      ),
                                    ),
                                    SizedBox(height: 5),
                                    Text(
                                      tanggalSelesai,
                                      style: TextStyle(
                                          color: Colors.white70,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                            ]))
                  ]),
            )))
          ]),
        ));
  }

  Widget listdataProduct(BuildContext context, data, id) {
    return InkWell(
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: data.length,
          itemBuilder: (BuildContext context, int j) {
            return data[j].status != 2
                ? Container(
                    color: Colors.white,
                    child: ListTile(
                      onTap: () {
                        return Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProductProgressUI(
                                    idOrder: id,
                                    idProduk: data[j].idProduct.toString())));
                      },
                      leading: new CircleAvatar(
                        backgroundColor: COLOR.primary(),
                        foregroundColor: Colors.white,
                        child: new Icon(Icons.list),
                      ),
                      title: new Text(
                        data[j].namaProduct,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      subtitle: new Text('ID PRODUCT #${data[j].idProduct}'),
                      trailing: new Text(
                          'Selesai : ${data[j].selesai}/${data[j].totalPekerjaan} '),
                    ),
                  )
                : Container();
          }),
    );
  }
}
