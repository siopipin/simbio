import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:intl/intl.dart';
import 'package:simbio/src/chat/ui/chat_private.dart';
import 'package:simbio/src/util/firebase_notification_handler.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/widgets/umum_widget.dart';
import 'package:simbio/src/order/blocs/order_blocs.dart';
import 'package:simbio/src/order/models/order_detail_scan.dart';
import 'package:simbio/src/order/screens/product_progress.dart';

class DetailOrderScanScreen extends StatefulWidget {
  final String id;

  DetailOrderScanScreen({Key key, this.id}) : super(key: key);

  _DetailOrderScanScreenState createState() => _DetailOrderScanScreenState();
}

class _DetailOrderScanScreenState extends State<DetailOrderScanScreen> {
  @override
  void initState() {
    super.initState();
    new FirebaseNotifications().setUpFirebase(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: UmumWidget.appBar(title: "Detail Order"),
      body: FutureBuilder(
          future: orderBLOC.doGetOrderScan(idOrder: widget.id),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return Text("No Connection");
              case ConnectionState.waiting:
              case ConnectionState.active:
                return Center(
                  child: CircularProgressIndicator(),
                );
              case ConnectionState.done:
                if (snapshot.hasError) {
                  return Center(
                    child: Text(snapshot.error.toString()),
                  );
                } else {
                  DetailOrderScanModel response =
                      DetailOrderScanModel.fromJson(snapshot.data.data);
                  return detailOrder(data: response);
                }
            }
          }),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (_) => ChatPrivate()));
        },
        label: Text("Chat"),
        icon: Icon(Icons.message),
        backgroundColor: COLOR.primary(),
        foregroundColor: Colors.white,
      ),
    );
  }

  Widget detailOrder({DetailOrderScanModel data}) {
    final status = data.status == 1 ? "On Progress" : "Selesai";
    //Format Tanggal
    var date = DateTime.parse(data.tanggalTargetSelesai);
    var formatter = new DateFormat('yyyy-MM-dd');
    String tanggalSelesai = formatter.format(date);

    var date2 = DateTime.parse(data.tanggal);
    var formatter2 = new DateFormat('yyyy-MM-dd');
    String tanggalMulai = formatter2.format(date2);

    return Container(
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.only(left: 10, top: 10),
              width: MediaQuery.of(context).size.width,
              color: COLOR.ketiga(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "${data.customer}",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  new Container(
                    padding: EdgeInsets.only(top: 40),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Row(
                          children: <Widget>[
                            new Expanded(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Text(
                                    "Pasien",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  new Text(
                                    "${data.namaPasien}",
                                    style: TextStyle(color: Colors.white),
                                  )
                                ],
                              ),
                            ),
                            new Expanded(
                              child: new Container(
                                padding: EdgeInsets.only(right: 10),
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    new Text(
                                      "Status",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    new Text(
                                      "$status",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 18),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  new Divider(
                    color: Colors.white,
                  ),
                  new Container(
                    padding: EdgeInsets.only(top: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Row(
                          children: <Widget>[
                            new Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  new Text(
                                    "Tanggal Mulai",
                                    style: TextStyle(
                                        color: Colors.white70, fontSize: 11),
                                  ),
                                  new Container(
                                    padding: EdgeInsets.only(top: 5),
                                    child: new Text(
                                      "$tanggalMulai",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            new Expanded(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  new Text("Tanggal Selesai",
                                      style: TextStyle(
                                          color: Colors.white70, fontSize: 10)),
                                  new Container(
                                    padding: EdgeInsets.only(top: 5),
                                    child: new Text(
                                      "$tanggalSelesai",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            new Expanded(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  new Text("Total Produk",
                                      style: TextStyle(
                                          color: Colors.white70, fontSize: 10)),
                                  new Container(
                                    padding: EdgeInsets.only(top: 5),
                                    child: new Text(
                                      "${data.products.length}",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
                padding: EdgeInsets.all(10.0),
                child: Scrollbar(
                  child: new ListView(
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.only(bottom: 20),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                                child: ListTile(
                              leading: Icon(Icons.indeterminate_check_box),
                              title: new Text(
                                "List Produk",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            )),
                            data.products.length != 0
                                ? listProduct(data)
                                : Container(
                                    padding: EdgeInsets.only(left: 70),
                                    child: Text("Produk tidak ditemukan"),
                                  ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
          )
        ],
      ),
    );
  }

  Widget listProduct(DetailOrderScanModel data) {
    return Scrollbar(
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: data.products.length,
        itemBuilder: (BuildContext context, int i) {
          return new Container(
              height: 100,
              margin: EdgeInsets.only(left: 14),
              child: GestureDetector(
                onTap: () => Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ProductProgressUI(
                        idOrder: widget.id,
                        idProduk: data.products[i].idProduct.toString()))),
                child: new Card(
                  child: new Container(
                      padding: EdgeInsets.all(5.0),
                      child: new Column(
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              new Expanded(
                                flex: 1,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      padding:
                                          EdgeInsets.only(left: 10, top: 15),
                                      child: CircleAvatar(
                                        backgroundColor: COLOR.primary(),
                                        foregroundColor: Colors.white,
                                        child: Text(
                                            '${data.products[i].namaProduct.substring(0, 1)}'),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              new Expanded(
                                flex: 3,
                                child: Container(
                                    padding: EdgeInsets.only(top: 15, left: 10),
                                    child: new Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        new Text(
                                          "${data.products[i].namaProduct}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 15),
                                        ),
                                        new Text(
                                            "Warna: ${data.products[i].selesai}"),
                                        new Text(
                                            "Susunan Gigi:${data.products[i].totalPekerjaan}"),
                                      ],
                                    )),
                              ),
                              new Expanded(
                                flex: 3,
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    new Text(
                                        "ID Product ${data.products[i].idProduct}",
                                        style: TextStyle(
                                          color: Colors.black54,
                                          fontSize: 11,
                                        ))
                                  ],
                                ),
                              )
                            ],
                          ),
                        ],
                      )),
                ),
              ));
        },
      ),
    );
  }
}
