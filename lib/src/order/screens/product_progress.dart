import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:intl/intl.dart';
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/chat/ui/chat_private.dart';
import 'package:simbio/src/order/models/status_quality_model.dart';
import 'package:simbio/src/util/firebase_notification_handler.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/widgets/umum_widget.dart';
import 'package:simbio/src/order/blocs/order_bloc.dart';
import 'package:simbio/src/order/blocs/order_logic.dart';
import 'package:simbio/src/order/models/progress_model.dart';
import 'package:meta/meta.dart';

class ProductProgressUI extends StatefulWidget {
  @required
  final String idOrder;
  @required
  final String idProduk;
  ProductProgressUI({this.idOrder, this.idProduk});
  @override
  _ProductProgressUIState createState() => _ProductProgressUIState();
}

class _ProductProgressUIState extends State<ProductProgressUI> {
  //Quality Controll
  bool pic = false;
  bool margin = false;
  bool oklusi = false;
  bool warna = false;
  bool anatomi = false;

  int picint, marginint, oksilusint, warnaint, anatomiint;

  int currentstep = 0;
  String step;
  bool done = false;
  String statusprivilege;
  final storage = new FlutterSecureStorage();

  //Data Progress
  ProgressModel dataProgress;
  StatusQualityControlModel statusQualityControlModel;
  bool statusCekQuality;

  @override
  void initState() {
    super.initState();
    new FirebaseNotifications().setUpFirebase(context);
    ordersBLOC.doGetProgress(
        idorder: widget.idOrder, idproduct: widget.idProduk);
    doCekquality();
    cekStatusQualityControl();
    doGetProgresFirst();
    initPrivilege();
  }

  doCekquality() async {
    statusQualityControlModel = await ordersBLOC.doCekQuality(
        idorder: widget.idOrder, idproduct: widget.idProduk);

    setState(() {
      statusCekQuality = statusQualityControlModel.status;
    });
  }

  doGetProgresFirst() async {
    print("GET PROGRESS FIRST");
    dataProgress = await ordersBLOC.doGetProgress(
        idorder: widget.idOrder, idproduct: widget.idProduk);
    int lastStep;
    int totalProgress = dataProgress.data.length;
    int ulang = 0;
    print("TOTAL PROGRESS : $totalProgress");
    for (int i = 0; i < dataProgress.data.length; i++) {
      if (dataProgress.data[i].status == 2 && ulang < totalProgress - 1) {
        print('JUMPA 2');
        currentstep = dataProgress.data[i].nomor;
        print("DALAM PERULANGAN : $currentstep");
        lastStep = currentstep;
        ulang += 1;
        print('INI ULANG: $ulang');
      } else if (dataProgress.data[i].status == 1) {
        currentstep = dataProgress.data[i].nomor - 1;
        print("DALAM PERULANGAN : $currentstep");
        lastStep = currentstep;
      } else if (dataProgress.data[i].status == 3) {
        currentstep = dataProgress.data[i].nomor - 1;
        print("DALAM PERULANGAN : $currentstep");
        lastStep = currentstep;
      } else if (dataProgress.data[i].status == 2 &&
          lastStep == dataProgress.data.length) {
        print('JUMPA 2 last');
        currentstep = dataProgress.data[i].nomor;
        print("DALAM PERULANGAN : $currentstep");
        lastStep = currentstep;
      }
    }
    print("INI CURRENT STEP KU: $lastStep");
    setState(() {
      dataProgress = dataProgress;
    });
  }

  void initPrivilege() async {
    String privilage = await AuthRepository().hasPrivilege();
    setState(() {
      statusprivilege = privilage;
    });
  }

  Future<void> lastProgress(int step) async {
    await storage.write(key: 'currentstep', value: step.toString());
  }

  Future<void> resetLastProgress() async {
    await storage.write(key: 'currentstep', value: "0");
  }

  Future<void> lastStatusQualityControl() async {
    await storage.write(key: 'done', value: done.toString());
  }

  Future cekStatusQualityControl() async {
    bool cek;
    print("CEK SAYA");
    dataProgress = await ordersBLOC.doGetProgress(
        idorder: widget.idOrder, idproduct: widget.idProduk);
    await new Future.delayed(const Duration(seconds: 1));
    for (int i = 0; i < dataProgress.data.length; i++) {
      if (dataProgress.data[i].status == 1 ||
          dataProgress.data[i].status == 0 ||
          dataProgress.data[i].status == 3 ||
          statusCekQuality == true) {
        cek = false;
      } else {
        cek = true;
      }
    }
    setState(() {
      done = cek;
      print('KETIKA SAYA CEK ADALAH : $done');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: UmumWidget.appBar(title: "Progress Product"),
      body: StreamBuilder(
        stream: ordersBLOC.streamDataProgress.stream,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            case ConnectionState.waiting:
              {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            case ConnectionState.active:
            case ConnectionState.done:
              return progressBody(snapshot.data);
            default:
              {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
          }
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (_) => ChatPrivate()));
        },
        label: Text("Chat"),
        icon: Icon(Icons.message),
        backgroundColor: COLOR.primary(),
        foregroundColor: Colors.white,
      ),
    );
  }

  Widget progressBody(ProgressModel data) {
    List<Step> progress = new List<Step>();
    for (var i = 0; i < data.data.length; i++) {
      // DateTime tgl = DateTime.parse(data.data[i].tanggalSelesai);
      // var formatter = new DateFormat('E, dd-MM-yyyy');
      // String formatted = formatter.format(tgl);
      progress.add(new Step(
          title: new Text(data.data[i].namaProses.toString()),
          subtitle: data.data[i].status == 0
              ? Text("Status: Pending")
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text('Status : ${data.data[i].statusText}'),
                    data.data[i].namaTekniker == null
                        ? Text("")
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                'Oleh: ${data.data[i].namaTekniker.toString()}',
                              )
                            ],
                          ),
                  ],
                ),
          isActive: (data.data[i].status == 3 &&
                      data.data[i].approveSpv == 1 &&
                      statusprivilege != "3" ||
                  data.data[i].status == 0)
              ? false
              : true,
          content: Text(""),
          state: (data.data[i].status == 0)
              ? StepState.indexed
              : (data.data[i].status == 1)
                  ? StepState.editing
                  : (data.data[i].status == 2)
                      ? StepState.disabled
                      : StepState.complete));
    }
    return Center(
      child: RefreshIndicator(
        child: Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 20, left: 20),
                width: MediaQuery.of(context).size.width,
                height: 80,
                color: Colors.grey[200],
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      "Progress Kerja",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.w700),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    new Text(
                      "ID Order : ${data.idorder}",
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 14,
                          fontWeight: FontWeight.w400),
                    ),
                    SizedBox(
                      height: 5,
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 5, right: 5, top: 10),
                width: MediaQuery.of(context).size.width,
                height: 50,
                child: new Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        decoration:
                            const BoxDecoration(color: Color(0xff8DB81D)),
                        padding: EdgeInsets.only(left: 10),
                        height: double.infinity,
                        width: double.infinity,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Text("Tahapan Kerja",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500))
                          ],
                        ),
                      ),
                      flex: 3,
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(right: 20),
                        decoration:
                            const BoxDecoration(color: Color(0xffB3D55C)),
                        width: double.infinity,
                        height: double.infinity,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            new Text("${data.namaProduct}",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500))
                          ],
                        ),
                      ),
                      flex: 3,
                    )
                  ],
                ),
              ),
              Expanded(
                  child: Scrollbar(
                child: ListView(
                  children: <Widget>[
                    Stepper(
                      physics: ClampingScrollPhysics(),
                      currentStep: currentstep,
                      steps: progress,
                      type: StepperType.vertical,
                      controlsBuilder: (BuildContext context,
                          {VoidCallback onStepContinue,
                          VoidCallback onStepCancel}) {
                        if (data.data[currentstep].status == 2 &&
                            data.data[currentstep].approveSpv == 1) {
                          lastProgress(currentstep + 1);
                        }

                        return Row(
                          children: <Widget>[
                            (data.data[currentstep].status == 2 &&
                                    data.data[currentstep].approveSpv == 0)
                                ? Container()
                                : (data.data[currentstep].status == 3 &&
                                        data.data[currentstep].approveSpv ==
                                            1 &&
                                        statusprivilege == "2")
                                    ? Text("Menunggu Approve Supervisor..")
                                    : (data.data[currentstep].status == 3 &&
                                            data.data[currentstep].approveSpv ==
                                                1 &&
                                            statusprivilege == "3")
                                        ? RaisedButton(
                                            hoverColor: Colors.blueAccent,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10)),
                                            onPressed: onStepContinue,
                                            color: COLOR.primary(),
                                            textColor: Colors.white,
                                            textTheme: ButtonTextTheme.normal,
                                            child: Text("APPROVE"))
                                        : (data.data[currentstep].status == 0 &&
                                                statusprivilege == "3")
                                            ? Text(
                                                "Menunggu Tekniker untuk Memulai..")
                                            : ((data.data[currentstep].status ==
                                                            2 &&
                                                        statusprivilege ==
                                                            "3") ||
                                                    (statusprivilege == "2" &&
                                                        data.data[currentstep]
                                                                .status ==
                                                            2))
                                                ? Container()
                                                : RaisedButton(
                                                    hoverColor:
                                                        Colors.blueAccent,
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10)),
                                                    onPressed: onStepContinue,
                                                    color: COLOR.primary(),
                                                    textColor: Colors.white,
                                                    textTheme:
                                                        ButtonTextTheme.normal,
                                                    child: (data
                                                                .data[
                                                                    currentstep]
                                                                .status ==
                                                            0)
                                                        ? Text("MULAI")
                                                        : (data.data[currentstep]
                                                                    .status ==
                                                                1)
                                                            ? Text("SELESAI")
                                                            : Text(""),
                                                  ),
                          ],
                        );
                      },
                      onStepContinue: () async {
                        String tmp = await AuthRepository().hasId();
                        String privilage =
                            await AuthRepository().hasPrivilege();
                        int id = int.parse(tmp);
                        int nextStatus = statusprivilege == "2"
                            ? data.data[currentstep].status + 1
                            : statusprivilege == "3"
                                ? data.data[currentstep].status - 1
                                : data.data[currentstep].status + 1;
                        int nextStatusApv = data.data[currentstep].status + 2;
                        DateTime formattedDate = DateTime.now();
                        String now =
                            DateFormat("yyyyMMddHHmmss").format(formattedDate);
                        setState(() {
                          if (data.data[currentstep].status == 0 &&
                              data.data[currentstep].approveSpv == 0) {
                            print("Status 0 dan approve = 0");
                            lastProgress(currentstep);
                            onLoading(
                                idorder: widget.idOrder,
                                idprodct: widget.idProduk,
                                context: context,
                                data: data,
                                currentstep: currentstep,
                                idpekerja: id,
                                nexstatus: nextStatus,
                                date: now);
                          } else if (data.data[currentstep].status == 1 &&
                              data.data[currentstep].approveSpv == 0 &&
                              currentstep != progress.length - 1) {
                            print("Status 1 dan apprv = 0");
                            onLoading(
                                idorder: widget.idOrder,
                                idprodct: widget.idProduk,
                                context: context,
                                data: data,
                                currentstep: currentstep,
                                idpekerja: id,
                                nexstatus: nextStatus,
                                date: now);
                            currentstep = currentstep + 1;
                            lastProgress(currentstep);
                          } else if (data.data[currentstep].status == 2 &&
                              data.data[currentstep].approveSpv == 0 &&
                              currentstep < progress.length - 1) {
                            onLoading(
                                idorder: widget.idOrder,
                                idprodct: widget.idProduk,
                                context: context,
                                data: data,
                                currentstep: currentstep,
                                idpekerja: id,
                                nexstatus: nextStatus,
                                date: now);
                            Future.delayed(Duration(seconds: 2));
                            currentstep = currentstep + 1;
                            lastProgress(currentstep);
                          } else if (data.data[currentstep].status == 0 &&
                              data.data[currentstep].approveSpv == 1) {
                            onLoading(
                                idorder: widget.idOrder,
                                idprodct: widget.idProduk,
                                context: context,
                                data: data,
                                currentstep: currentstep,
                                idpekerja: id,
                                nexstatus: nextStatus,
                                date: now);
                            lastProgress(currentstep);
                          } else if (data.data[currentstep].status == 1 &&
                              data.data[currentstep].approveSpv == 1) {
                            onLoading(
                                idorder: widget.idOrder,
                                idprodct: widget.idProduk,
                                context: context,
                                data: data,
                                currentstep: currentstep,
                                idpekerja: id,
                                nexstatus: nextStatusApv,
                                date: now);
                            lastProgress(currentstep);
                            doNotifSPV(idorder: widget.idOrder);
                          } else if (data.data[currentstep].status == 2 &&
                              data.data[currentstep].approveSpv == 1) {
                            onLoading(
                                idorder: widget.idOrder,
                                idprodct: widget.idProduk,
                                context: context,
                                data: data,
                                currentstep: currentstep,
                                idpekerja: id,
                                nexstatus: nextStatus,
                                date: now);
                            lastProgress(currentstep);
                          } else if (data.data[currentstep].status == 3 &&
                              data.data[currentstep].approveSpv == 1 &&
                              currentstep < progress.length - 1) {
                            onLoading(
                                idorder: widget.idOrder,
                                idprodct: widget.idProduk,
                                context: context,
                                data: data,
                                currentstep: currentstep,
                                idpekerja: id,
                                nexstatus: nextStatus,
                                date: now);

                            statusprivilege == "2"
                                ? currentstep = currentstep
                                : (privilage == "3")
                                    ? currentstep = currentstep + 1
                                    : currentstep = currentstep + 1;
                            lastProgress(currentstep);
                          } else if (data.data[currentstep].status == 3 &&
                              data.data[currentstep].approveSpv == 1 &&
                              currentstep == progress.length - 1) {
                            onLoading(
                                idorder: widget.idOrder,
                                idprodct: widget.idProduk,
                                context: context,
                                data: data,
                                currentstep: currentstep,
                                idpekerja: id,
                                nexstatus: nextStatus,
                                date: now);

                            statusprivilege == "2"
                                ? currentstep = currentstep
                                : (privilage == "3")
                                    ? currentstep = currentstep
                                    : currentstep = currentstep;
                            lastProgress(currentstep);
                            setState(() {
                              done = true;
                              lastStatusQualityControl();
                            });
                          } else if (data.data[currentstep].status == 1 &&
                              data.data[currentstep].approveSpv == 0 &&
                              currentstep == progress.length - 1) {
                            print(
                                'currentstep terakhir $currentstep | progress length: ${progress.length}');
                            onLoading(
                                idorder: widget.idOrder,
                                idprodct: widget.idProduk,
                                context: context,
                                data: data,
                                currentstep: currentstep,
                                idpekerja: id,
                                nexstatus: nextStatus,
                                date: now);
                            lastProgress(currentstep);
                            setState(() {
                              done = true;
                              lastStatusQualityControl();
                            });
                          } else if (data.data[currentstep].status == 3 &&
                              data.data[currentstep].approveSpv == 1 &&
                              currentstep == progress.length - 1) {}
                        });
                      },
                    ),
                    done == true ? qualityControl(context) : Container()
                  ],
                ),
              ))
            ],
          ),
        ),
        onRefresh: () => ordersBLOC.doGetProgress(
            idorder: widget.idOrder, idproduct: widget.idProduk),
      ),
    );
  }

  Widget qualityControl(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: 40,
            color: COLOR.primary(),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Quality Control",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.white),
                )
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          CheckboxListTile(
            title: Text("Margin"),
            value: margin,
            secondary: new Icon(Icons.photo_size_select_large),
            activeColor: COLOR.primary(),
            onChanged: (bool value) {
              setState(() {
                margin = value;
                if (margin == true) {
                  marginint = 1;
                } else if (margin == null) {
                  marginint = 0;
                } else {
                  marginint = 0;
                }
              });
            },
          ),
          Divider(),
          CheckboxListTile(
            title: Text("Oklusi"),
            value: oklusi,
            secondary: new Icon(Icons.colorize),
            activeColor: COLOR.primary(),
            onChanged: (bool value) {
              setState(() {
                oklusi = value;
                if (oklusi == true) {
                  oksilusint = 1;
                } else if (oklusi == null) {
                  oksilusint = 0;
                } else {
                  oksilusint = 0;
                }
              });
            },
          ),
          Divider(),
          CheckboxListTile(
            title: Text("Anatomy"),
            value: anatomi,
            secondary: new Icon(Icons.pan_tool),
            activeColor: COLOR.primary(),
            onChanged: (bool value) {
              setState(() {
                anatomi = value;
                if (anatomi == true) {
                  anatomiint = 1;
                } else if (anatomiint == null) {
                  anatomiint = 0;
                } else {
                  anatomiint = 0;
                }
              });
            },
          ),
          Divider(),
          CheckboxListTile(
            title: Text("Warna"),
            value: warna,
            secondary: new Icon(Icons.color_lens),
            activeColor: COLOR.primary(),
            onChanged: (bool value) {
              setState(() {
                warna = value;
                if (warna == true) {
                  warnaint = 1;
                } else if (warna == null) {
                  warnaint = 0;
                } else {
                  warnaint = 0;
                }
              });
            },
          ),
          Divider(),
          CheckboxListTile(
            title: Text("Pic"),
            value: pic,
            secondary: new Icon(Icons.picture_in_picture_alt),
            activeColor: COLOR.primary(),
            onChanged: (bool value) {
              setState(() {
                print('ini value : $value');
                pic = value;
                if (pic == true) {
                  picint = 1;
                } else if (pic == null) {
                  picint = 0;
                } else {
                  picint = 0;
                }
              });
            },
          ),
          Container(
            padding: EdgeInsets.only(left: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  color: COLOR.primary(),
                  onPressed: () async {
                    DateTime formattedDate = DateTime.now();
                    String now =
                        DateFormat("yyyyMMddHHmmss").format(formattedDate);
                    await doUpdateQualityControl(
                        context: context,
                        anatomi: anatomiint != null ? anatomiint : 0,
                        idorder: int.parse(widget.idOrder),
                        idproduct: int.parse(widget.idProduk),
                        margin: marginint != null ? marginint : 0,
                        oksilus: oksilusint != null ? oksilusint : 0,
                        pic: picint != null ? picint : 0,
                        warna: warnaint != null ? warnaint : 0,
                        tanggal: now);

                    setState(() {
                      done = false;
                      resetLastProgress();
                    });
                  },
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
