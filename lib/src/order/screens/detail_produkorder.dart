import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:simbio/src/chat/ui/chat_private.dart';
import 'package:simbio/src/order/models/tekniker_order_model.dart';
import 'package:simbio/src/util/firebase_notification_handler.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/widgets/umum_widget.dart';
import 'package:simbio/src/order/blocs/order_bloc.dart';
import 'package:simbio/src/order/models/order_detail_model.dart';
import 'package:simbio/src/order/screens/product_progress.dart';

class DetailProdukOrderScreen extends StatefulWidget {
  final String id;

  DetailProdukOrderScreen({Key key, this.id}) : super(key: key);

  _DetailProdukOrderScreenState createState() =>
      _DetailProdukOrderScreenState();
}

class _DetailProdukOrderScreenState extends State<DetailProdukOrderScreen> {
  PegawaiinOrderModel pegawaiinOrderModel;

  @override
  void initState() {
    super.initState();
    new FirebaseNotifications().setUpFirebase(context);
    ordersBLOC.doGetOrderDetail(idOrder: widget.id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: UmumWidget.appBar(title: "Detail Order"),
      body: StreamBuilder(
        stream: ordersBLOC.streamDataDetailOrder.stream,
        builder:
            (BuildContext context, AsyncSnapshot<OrderDetailModel> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            case ConnectionState.waiting:
              {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            case ConnectionState.active:
            case ConnectionState.done:
              return detailOrder(data: snapshot.data);

            default:
              {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
          }
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (_) => ChatPrivate()));
        },
        label: Text("Chat"),
        icon: Icon(Icons.message),
        backgroundColor: COLOR.primary(),
        foregroundColor: Colors.white,
      ),
    );
  }

  Widget detailOrder({OrderDetailModel data}) {
    final status = data.status == 1 ? "On Progress" : "Selesai";
    //Format Tanggal
    var date = DateTime.parse(data.tanggalTargetSelesai);
    var formatter = new DateFormat('yyyy-MM-dd');
    String tanggalSelesai = formatter.format(date);

    var date2 = DateTime.parse(data.tanggal);
    var formatter2 = new DateFormat('yyyy-MM-dd');
    String tanggalMulai = formatter2.format(date2);

    return Container(
        child: Center(
      child: RefreshIndicator(
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.only(left: 10, top: 10),
                width: MediaQuery.of(context).size.width,
                color: COLOR.secondary(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${data.customer}",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        ),
                        new Text(
                          "Email : ${data.email}",
                          style: TextStyle(color: Colors.white70, fontSize: 12),
                        )
                      ],
                    ),
                    new Container(
                      padding: EdgeInsets.only(top: 40),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              new Expanded(
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Text(
                                      "Pasien",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    new Text(
                                      "${data.namaPasien}",
                                      style: TextStyle(color: Colors.white),
                                    )
                                  ],
                                ),
                              ),
                              new Expanded(
                                child: new Container(
                                  padding: EdgeInsets.only(right: 10),
                                  child: new Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: <Widget>[
                                      new Text(
                                        "Status",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      new Text(
                                        "$status",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 18),
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    new Divider(
                      color: Colors.white,
                    ),
                    new Container(
                      padding: EdgeInsets.only(top: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              new Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Text(
                                      "Tanggal Mulai",
                                      style: TextStyle(
                                          color: Colors.white70, fontSize: 11),
                                    ),
                                    new Container(
                                      padding: EdgeInsets.only(top: 5),
                                      child: new Text(
                                        "$tanggalMulai",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 12),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              new Expanded(
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Text("Tanggal Selesai",
                                        style: TextStyle(
                                            color: Colors.white70,
                                            fontSize: 10)),
                                    new Container(
                                      padding: EdgeInsets.only(top: 5),
                                      child: new Text(
                                        "$tanggalSelesai",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 12),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              new Expanded(
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Text("Total Produk",
                                        style: TextStyle(
                                            color: Colors.white70,
                                            fontSize: 10)),
                                    new Container(
                                      padding: EdgeInsets.only(top: 5),
                                      child: new Text(
                                        "${data.details.length}",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 12),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Scrollbar(
                    child: new ListView(
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      children: <Widget>[
                        data.details.length > 0
                            ? new Container(
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                        child: ListTile(
                                      leading:
                                          Icon(Icons.indeterminate_check_box),
                                      title: new Text(
                                        "List Produk",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    )),
                                    listProduct(data)
                                  ],
                                ),
                              )
                            : Container(
                                padding: EdgeInsets.only(top: 40),
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(
                                        Icons.dvr,
                                        color: COLOR.ketiga(),
                                        size: 70,
                                      ),
                                      Text(
                                        "Produk tidak ditemukan",
                                      )
                                    ],
                                  ),
                                ),
                              ),
                      ],
                    ),
                  )),
            )
          ],
        ),
        onRefresh: () => ordersBLOC.doGetOrderDetail(idOrder: widget.id),
      ),
    ));
  }

  Widget listProduct(OrderDetailModel data) {
    return Scrollbar(
      child: ListView.builder(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemCount: data.details.length,
        itemBuilder: (BuildContext context, int i) {
          return new Container(
              height: 100,
              margin: EdgeInsets.only(left: 14),
              child: GestureDetector(
                onTap: () => Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ProductProgressUI(
                        idOrder: widget.id,
                        idProduk: data.details[i].idProduct.toString()))),
                child: new Card(
                  child: new Container(
                      padding: EdgeInsets.all(5.0),
                      child: new Column(
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              new Expanded(
                                flex: 1,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      padding:
                                          EdgeInsets.only(left: 10, top: 15),
                                      child: CircleAvatar(
                                        backgroundColor: COLOR.primary(),
                                        foregroundColor: Colors.white,
                                        child: Text(
                                            '${data.details[i].namaProduct.substring(0, 1)}'),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              new Expanded(
                                flex: 4,
                                child: Container(
                                    padding: EdgeInsets.only(top: 15, left: 10),
                                    child: new Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        new Text(
                                          "${data.details[i].namaProduct}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 15),
                                        ),
                                        new Text(
                                            "Warna: ${data.details[i].warna}"),
                                        new Text(
                                            "Susunan Gigi:${data.details[i].posisiGigi}"),
                                      ],
                                    )),
                              ),
                            ],
                          ),
                        ],
                      )),
                ),
              ));
        },
      ),
    );
  }
}
