import 'package:flutter/material.dart';
import 'package:simbio/src/order/blocs/order_bloc.dart';
import 'package:simbio/src/order/blocs/order_logic.dart';
import 'package:simbio/src/order/models/orders_model.dart';
import 'package:simbio/src/order/screens/detail_produkorder.dart';
import 'package:simbio/src/util/firebase_notification_handler.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/widgets/umum_widget.dart';

class OrderTotalUI extends StatefulWidget {
  OrderTotalUI({Key key}) : super(key: key);

  _OrderTotalUIState createState() => _OrderTotalUIState();
}

class _OrderTotalUIState extends State<OrderTotalUI> {
  @override
  void initState() {
    super.initState();
    new FirebaseNotifications().setUpFirebase(context);
    ordersBLOC.doGetOrder();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: UmumWidget.appBar(title: "Semua Order"),
      body: StreamBuilder(
        stream: ordersBLOC.streamDataOrder.stream,
        builder: (BuildContext context, AsyncSnapshot<OrdersModel> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            case ConnectionState.waiting:
              {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            case ConnectionState.active:
            case ConnectionState.done:
              return onRefresh(context, snapshot.data);
            default:
              {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
          }
        },
      ),
    );
  }

  Widget onRefresh(BuildContext context, OrdersModel data) {
    return Center(
      child: RefreshIndicator(
        child: onBody(context, data),
        onRefresh: () => ordersBLOC.doGetOrder(),
      ),
    );
  }

  Widget onBody(BuildContext context, OrdersModel data) {
    return data.orders.length > 0
        ? Container(child: onListOrderHistory(context, data))
        : Container(
            padding: EdgeInsets.only(bottom: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.dvr,
                  color: COLOR.ketiga(),
                  size: 70,
                ),
                Text(
                  "Riwayat Order tidak ditemukan",
                )
              ],
            ),
          );
  }

  Widget onListOrderHistory(BuildContext context, OrdersModel data) {
    return Scrollbar(
      child: ListView.builder(
        itemCount: data.orders.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
              onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DetailProdukOrderScreen(
                            id: data.orders[index].id.toString(),
                          ))),
              child: Card(
                child: ListTile(
                  leading: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.account_box,
                          color: COLOR.primary(),
                          size: 50,
                        )
                      ],
                    ),
                  ),
                  title: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Text(data.orders[index].idOrder),
                      ),
                      Container(
                        width: 70,
                          child: data.orders[index].status == 1
                              ? Container(
                                padding: EdgeInsets.only(top: 2, bottom: 2, left: 5, right: 2),
                                  color: COLOR.primary(),
                                  child: Text(
                                    doCekStatus(
                                        data.orders[index].status.toString()),
                                    style: TextStyle(
                                        fontSize: 11, color: Colors.white),
                                  ),
                                )
                              : Container(
                                padding: EdgeInsets.only(top: 2, bottom: 2, left: 5, right: 2),
                                  color: COLOR.ketiga(),
                                  child: Text(
                                    doCekStatus(
                                        data.orders[index].status.toString()),
                                    style: TextStyle(
                                        fontSize: 11, color: Colors.white),
                                  ),
                                ))
                    ],
                  ),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(data.orders[index].customer),
                    ],
                  ),
                ),
              ));
        },
      ),
    );
  }
}
