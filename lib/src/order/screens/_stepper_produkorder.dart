import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/landing/landing_screen.dart';
import 'package:simbio/src/widgets/umum_widget.dart';
import 'package:simbio/src/order/blocs/order_blocs.dart';
import 'package:simbio/src/order/models/progress_model.dart';

class StepperProdukScreen extends StatefulWidget {
  final String idOrder;
  final String idProduk;
  StepperProdukScreen({Key key, this.idOrder, this.idProduk}) : super(key: key);

  _StepperProdukScreenState createState() => _StepperProdukScreenState();
}

class _StepperProdukScreenState extends State<StepperProdukScreen> {
  int currentstep = 0;
  final val = false;
  @override
  Widget build(BuildContext context) {
    print('${widget.idOrder} dan ${widget.idProduk}');
    return Scaffold(
        appBar: UmumWidget.appBar(title: "Progress Produk"),
        body: FutureBuilder<ProgressModel>(
          future: orderBLOC.doGetProgress(
              idOrder: widget.idOrder, idProduct: widget.idProduk),
          builder: (BuildContext context, AsyncSnapshot data) {
            if (data.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else if (data.connectionState == ConnectionState.done) {
              if (data.hasError) {
                return Container(
                  padding: EdgeInsets.all(20),
                  child: Card(
                      child: ListTile(
                    title: Text(
                      "Notification",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                    ),
                    subtitle: Text(
                      data.error.toString(),
                      style: TextStyle(fontSize: 9, color: Colors.grey),
                    ),
                  )),
                );
              } else if (data.hasData) {
                return progressProduct(data: data.data);
              } else {
                return Center(
                  child: Center(
                    child: Text("Cannot Get data"),
                  ),
                );
              }
            }
            return new Center(
              child: new CircularProgressIndicator(),
            );
          },
        ));
  }

  Widget progressProduct({ProgressModel data}) {
    List<Step> progress = new List<Step>();
    for (var i = 0; i < data.data.length; i++) {
      progress.add(new Step(
          title: new Text(data.data[i].namaProses.toString()),
          subtitle: new Text(data.data[i].statusText.toString()),
          content: new Text(
            'Oleh: ${data.data[i].namaTekniker.toString()}',
            style: TextStyle(fontSize: 14),
          ),
          state: (data.data[i].status == 0)
              ? StepState.indexed
              : (data.data[i].status == 1)
                  ? StepState.editing
                  : (data.data[i].status == 2)
                      ? StepState.disabled
                      : StepState.complete));
    }
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 20, left: 20),
            width: MediaQuery.of(context).size.width,
            height: 80,
            color: Colors.grey[200],
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text(
                  "Progress Kerja",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.w700),
                ),
                SizedBox(
                  height: 5,
                ),
                new Text(
                  "ID Order : ${data.idorder}",
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                ),
                SizedBox(
                  height: 5,
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 5, right: 5, top: 10),
            width: MediaQuery.of(context).size.width,
            height: 50,
            child: new Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    decoration: const BoxDecoration(color: Color(0xff8DB81D)),
                    padding: EdgeInsets.only(left: 10),
                    height: double.infinity,
                    width: double.infinity,
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Text("Tahapan Kerja",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500))
                      ],
                    ),
                  ),
                  flex: 3,
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(right: 20),
                    decoration: const BoxDecoration(color: Color(0xffB3D55C)),
                    width: double.infinity,
                    height: double.infinity,
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        new Text("${data.namaProduct}",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500))
                      ],
                    ),
                  ),
                  flex: 3,
                )
              ],
            ),
          ),
          Expanded(
              flex: 1,
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: Scrollbar(
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      Container(
                          child: new Stepper(
                        currentStep: this.currentstep,
                        steps: progress,
                        type: StepperType.vertical,
                        onStepTapped: (step) {
                          setState(() {
                            currentstep = step;
                          });
                          print("onStepTapped : " + step.toString());
                        },
                        onStepContinue: () async {
                          String tmp = await AuthRepository().hasId();
                          int id = int.parse(tmp);
                          int nextStatus = data.data[currentstep].status + 1;
                          DateTime formattedDate = DateTime.now();
                          String now = DateFormat("yyyyMMddHHmmss")
                              .format(formattedDate);
                          setState(() {
                            if (data.data[currentstep].status == 0 &&
                                data.data[currentstep].approveSpv == 0) {
                              orderBLOC.doUpdateProgress(
                                  idorder: data.data[currentstep].id,
                                  idproduk: data.data[currentstep].idProduct,
                                  idtekniker: id,
                                  status: nextStatus,
                                  tanggalselesai: now.toString(),
                                  idprogress: data.data[currentstep].id);
                            } else if (data.data[currentstep].status == 1 &&
                                data.data[currentstep].approveSpv == 0) {
                              orderBLOC.doUpdateProgress(
                                  idorder: data.data[currentstep].id,
                                  idproduk: data.data[currentstep].idProduct,
                                  idtekniker: id,
                                  status: nextStatus,
                                  tanggalselesai: now.toString(),
                                  idprogress: data.data[currentstep].id);
                            } else if (data.data[currentstep].status == 2 &&
                                data.data[currentstep].approveSpv == 0 &&
                                currentstep < progress.length - 1) {
                              orderBLOC.doUpdateProgress(
                                  idorder: data.data[currentstep].id,
                                  idproduk: data.data[currentstep].idProduct,
                                  idtekniker: id,
                                  status: 2,
                                  tanggalselesai: now.toString(),
                                  idprogress: data.data[currentstep].id);
                              currentstep = currentstep + 1;
                            }
                            if (data.data[currentstep].status == 0 &&
                                data.data[currentstep].approveSpv == 1) {
                              orderBLOC.doUpdateProgress(
                                  idorder: data.data[currentstep].id,
                                  idproduk: data.data[currentstep].idProduct,
                                  idtekniker: id,
                                  status: nextStatus,
                                  tanggalselesai: now.toString(),
                                  idprogress: data.data[currentstep].id);
                            } else if (data.data[currentstep].status == 1 &&
                                data.data[currentstep].approveSpv == 1) {
                              orderBLOC.doUpdateProgress(
                                  idorder: data.data[currentstep].id,
                                  idproduk: data.data[currentstep].idProduct,
                                  idtekniker: id,
                                  status: nextStatus,
                                  tanggalselesai: now.toString(),
                                  idprogress: data.data[currentstep].id);
                            } else if (data.data[currentstep].status == 2 &&
                                data.data[currentstep].approveSpv == 1) {
                              orderBLOC.doUpdateProgress(
                                  idorder: data.data[currentstep].id,
                                  idproduk: data.data[currentstep].idProduct,
                                  idtekniker: id,
                                  status: nextStatus,
                                  tanggalselesai: now.toString(),
                                  idprogress: data.data[currentstep].id);
                            } else if (data.data[currentstep].status == 3 &&
                                data.data[currentstep].approveSpv == 1) {
                              orderBLOC.doUpdateProgress(
                                  idorder: data.data[currentstep].id,
                                  idproduk: data.data[currentstep].idProduct,
                                  idtekniker: id,
                                  status: 3,
                                  tanggalselesai: now.toString(),
                                  idprogress: data.data[currentstep].id);
                              currentstep = currentstep + 1;
                            } else if (data.data[currentstep].status == 2 &&
                                data.data[currentstep].approveSpv == 0 &&
                                currentstep == progress.length - 1) {
                              Future.delayed(
                                  Duration.zero, () => showToRefresh(context));
                            } else if (data.data[currentstep].status == 3 &&
                                data.data[currentstep].approveSpv == 1 &&
                                currentstep == progress.length - 1) {
                              Future.delayed(
                                  Duration.zero, () => showToRefresh(context));
                            }
                          });
                        },
                      )),
                      Container(
                        padding: EdgeInsets.only(top: 20, left: 20),
                        width: MediaQuery.of(context).size.width,
                        height: 100,
                        color: Colors.grey[200],
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text(
                              "Progress Kerja",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            new Text(
                              "ORDER ID : ${widget.idOrder}",
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )),
        ],
      ),
    );
 
  }

  void showToRefresh(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Quality Controll'),
            content: Container(
              height: 250,
              child: Column(
                children: <Widget>[
                  CheckboxListTile(
                    title: Text("Margin"),
                    value: val,
                    onChanged: (bool val) {
                      setState(() {
                        val = true;
                      });
                    },
                  ),
                  CheckboxListTile(
                    title: Text("Oklusi"),
                    value: val,
                    onChanged: (bool val) {
                      setState(() {
                        val = true;
                      });
                    },
                  ),
                  CheckboxListTile(
                    title: Text("Warna"),
                    value: val,
                    onChanged: (bool val) {
                      setState(() {
                        val = true;
                      });
                    },
                  ),
                  CheckboxListTile(
                    title: Text("Pic"),
                    value: val,
                    onChanged: (bool val) {
                      setState(() {
                        val = true;
                      });
                    },
                  )
                ],
              ),
            ),
            actions: <Widget>[
              new FlatButton(
                child: new Text('Agree'),
                onPressed: () {
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (_) => LandingScreen()));
                },
              )
            ],
          );
        });
  }

}
