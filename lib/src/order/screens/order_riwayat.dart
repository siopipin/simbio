import 'package:flutter/material.dart';
import 'package:simbio/src/order/blocs/order_bloc.dart';
import 'package:simbio/src/order/blocs/order_logic.dart';
import 'package:simbio/src/order/models/orders_riwayat_model.dart';
import 'package:simbio/src/order/screens/detail_produkorder.dart';
import 'package:simbio/src/util/firebase_notification_handler.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/widgets/umum_widget.dart';

class OrderRiwayatUI extends StatefulWidget {
  OrderRiwayatUI({Key key}) : super(key: key);

  _OrderRiwayatUIState createState() => _OrderRiwayatUIState();
}

class _OrderRiwayatUIState extends State<OrderRiwayatUI> {
  @override
  void initState() {
    super.initState();
    new FirebaseNotifications().setUpFirebase(context);
    ordersBLOC.doGetOrderHistory();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: UmumWidget.appBar(title: "Riwayat Order"),
      body: StreamBuilder(
        stream: ordersBLOC.streamDataOrderHistory.stream,
        builder:
            (BuildContext context, AsyncSnapshot<OrdersRiwayatModel> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            case ConnectionState.waiting:
              {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            case ConnectionState.active:
            case ConnectionState.done:
              return onRefresh(context, snapshot.data);

            default:
              {
                return Center(
                  child: CircularProgressIndicator(),
                );
            }
          }
        },
      ),
    );
  }

  Widget onRefresh(BuildContext context, OrdersRiwayatModel data) {
    return Center(
      child: RefreshIndicator(
        child: onBody(context, data),
        onRefresh: () => ordersBLOC.doGetOrderHistory(),
      ),
    );
  }

  Widget onBody(BuildContext context, OrdersRiwayatModel data) {
    return data.orders.length > 0
        ? Container(child: onListOrderHistory(context, data))
        : Container(
            padding: EdgeInsets.only(bottom: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.dvr,
                  color: COLOR.ketiga(),
                  size: 70,
                ),
                Text(
                  "Riwayat Order tidak ditemukan",
                )
              ],
            ),
          );
  }

  Widget onListOrderHistory(BuildContext context, OrdersRiwayatModel data) {
    return Scrollbar(
      child: ListView.builder(
        itemCount: data.orders.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
              onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DetailProdukOrderScreen(
                            id: data.orders[index].id.toString(),
                          ))),
              child: Card(
                child: ListTile(
                  leading: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.account_box,
                          color: COLOR.primary(),
                          size: 50,
                        )
                      ],
                    ),
                  ),
                  title: Text(data.orders[index].idOrder),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(data.orders[index].customer),
                      Text(
                          "Selesai Pada: ${doFormatTanggal(data.orders[index].tanggal)}")
                    ],
                  ),
                ),
              ));
        },
      ),
    );
  }
}
