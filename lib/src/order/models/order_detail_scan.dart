class DetailOrderScanModel {
  int id;
  String idOrder;
  int idCustomer;
  String namaPasien;
  String customer;
  String tanggal;
  String tanggalTargetSelesai;
  int status;
  List<Products> products;

  DetailOrderScanModel(
      {this.id,
      this.idOrder,
      this.idCustomer,
      this.namaPasien,
      this.customer,
      this.tanggal,
      this.tanggalTargetSelesai,
      this.status,
      this.products});

  DetailOrderScanModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idOrder = json['id_order'];
    idCustomer = json['id_customer'];
    namaPasien = json['nama_pasien'];
    customer = json['customer'];
    tanggal = json['tanggal'];
    tanggalTargetSelesai = json['tanggal_target_selesai'];
    status = json['status'];
    if (json['products'] != null) {
      products = new List<Products>();
      json['products'].forEach((v) {
        products.add(new Products.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_order'] = this.idOrder;
    data['id_customer'] = this.idCustomer;
    data['nama_pasien'] = this.namaPasien;
    data['customer'] = this.customer;
    data['tanggal'] = this.tanggal;
    data['tanggal_target_selesai'] = this.tanggalTargetSelesai;
    data['status'] = this.status;
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Products {
  int idProduct;
  String namaProduct;
  int selesai;
  int totalPekerjaan;

  Products(
      {this.idProduct, this.namaProduct, this.selesai, this.totalPekerjaan});

  Products.fromJson(Map<String, dynamic> json) {
    idProduct = json['id_product'];
    namaProduct = json['nama_product'];
    selesai = json['selesai'];
    totalPekerjaan = json['total_pekerjaan'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_product'] = this.idProduct;
    data['nama_product'] = this.namaProduct;
    data['selesai'] = this.selesai;
    data['total_pekerjaan'] = this.totalPekerjaan;
    return data;
  }
}