class PegawaiinOrderModel {
  int status;
  int statuscode;
  String message;
  List<Data> data;

  PegawaiinOrderModel({this.status, this.data, this.statuscode, this.message});

  PegawaiinOrderModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    statuscode = json['statuscode'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['statuscode'] = this.statuscode;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String nama;
  int privilege;
  String fcmtoken;

  Data({this.id, this.nama, this.privilege, this.fcmtoken});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nama = json['nama'];
    privilege = json['privilege'];
    fcmtoken = json['fcmtoken'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nama'] = this.nama;
    data['privilege'] = this.privilege;
    data['fcmtoken'] = this.fcmtoken;
    return data;
  }
}
