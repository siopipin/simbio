class ProgressModel {
  var msg;
  String idorder;
  String namaProduct;
  int statusCode;
  List<Data> data;

  ProgressModel({this.msg, this.statusCode, this.idorder, this.namaProduct, this.data});

  ProgressModel.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    idorder = json['idorder'];
    namaProduct = json['nama_product'];
    statusCode = json['statusCode'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    data['idorder'] = this.idorder;
    data['nama_product'] = this.namaProduct;
    data['statusCode'] = this.statusCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int idprogress;
  String idOrder;
  String nama;
  int id;
  int idProduct;
  int status;
  var tanggalSelesai;
  String namaProses;
  int approveSpv;
  int nomor;
  String statusText;
  var namaTekniker;

  Data(
      {this.idprogress,
      this.idOrder,
      this.nama,
      this.id,
      this.idProduct,
      this.status,
      this.tanggalSelesai,
      this.namaProses,
      this.approveSpv,
      this.nomor,
      this.statusText,
      this.namaTekniker});

  Data.fromJson(Map<String, dynamic> json) {
    idprogress = json['idprogress'];
    idOrder = json['id_order'];
    nama = json['nama'];
    id = json['id'];
    idProduct = json['id_product'];
    status = json['status'];
    tanggalSelesai = json['tanggal_selesai'];
    namaProses = json['nama_proses'];
    approveSpv = json['approve_spv'];
    nomor = json['nomor'];
    statusText = json['status_text'];
    namaTekniker = json['nama_tekniker'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idprogress'] = this.idprogress;
    data['id_order'] = this.idOrder;
    data['nama'] = this.nama;
    data['id'] = this.id;
    data['id_product'] = this.idProduct;
    data['status'] = this.status;
    data['tanggal_selesai'] = this.tanggalSelesai;
    data['nama_proses'] = this.namaProses;
    data['approve_spv'] = this.approveSpv;
    data['nomor'] = this.nomor;
    data['status_text'] = this.statusText;
    data['nama_tekniker'] = this.namaTekniker;
    return data;
  }
}