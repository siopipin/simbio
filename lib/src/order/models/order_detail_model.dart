class OrderDetailModel {
  int id;
  int statusCode;
  String msg;
  String idOrder;
  int idCustomer;
  String namaPasien;
  String customer;
  String tanggal;
  String email;
  String alamat;
  String tanggalTargetSelesai;
  int status;
  List<Details> details;

  OrderDetailModel(
      {this.id,
      this.idOrder,
      this.idCustomer,
      this.namaPasien,
      this.customer,
      this.tanggal,
      this.statusCode,
      this.msg,
      this.email,
      this.alamat,
      this.tanggalTargetSelesai,
      this.status,
      this.details});

  OrderDetailModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idOrder = json['id_order'];
    idCustomer = json['id_customer'];
    namaPasien = json['nama_pasien'];
    customer = json['customer'];
    tanggal = json['tanggal'];
    email = json['email'];
    alamat = json['alamat'];
    statusCode = json['statusCode'];
    msg = json['msg'];
    tanggalTargetSelesai = json['tanggal_target_selesai'];
    status = json['status'];
    if (json['details'] != null) {
      details = new List<Details>();
      json['details'].forEach((v) {
        details.add(new Details.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_order'] = this.idOrder;
    data['id_customer'] = this.idCustomer;
    data['nama_pasien'] = this.namaPasien;
    data['email'] = this.email;
    data['alamat'] = this.alamat;
    data['customer'] = this.customer;
    data['tanggal'] = this.tanggal;
    data['statusCode'] = this.statusCode;
    data['msg'] = this.msg;
    data['tanggal_target_selesai'] = this.tanggalTargetSelesai;
    data['status'] = this.status;
    if (this.details != null) {
      data['details'] = this.details.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Details {
  int idProduct;
  String namaProduct;
  String warna;
  String posisiGigi;

  Details({this.idProduct, this.namaProduct, this.warna, this.posisiGigi});

  Details.fromJson(Map<String, dynamic> json) {
    idProduct = json['id_product'];
    namaProduct = json['nama_product'];
    warna = json['warna'];
    posisiGigi = json['posisi_gigi'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_product'] = this.idProduct;
    data['nama_product'] = this.namaProduct;
    data['warna'] = this.warna;
    data['posisi_gigi'] = this.posisiGigi;
    return data;
  }
}
