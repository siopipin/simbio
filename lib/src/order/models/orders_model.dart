class OrdersModel {
  String message;
  int statuscode;
  List<Orders> orders;

  OrdersModel({this.message, this.statuscode, this.orders});

  OrdersModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    statuscode = json['statuscode'];
    if (json['orders'] != null) {
      orders = new List<Orders>();
      json['orders'].forEach((v) {
        orders.add(new Orders.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['statuscode'] = this.statuscode;
    if (this.orders != null) {
      data['orders'] = this.orders.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Orders {
  int privilege;
  int id;
  String idOrder;
  int idCustomer;
  String namaPasien;
  String customer;
  String tanggal;
  String tanggalTargetSelesai;
  int status;
  List<Products> products;

  Orders(
      {this.privilege,
      this.id,
      this.idOrder,
      this.idCustomer,
      this.namaPasien,
      this.customer,
      this.tanggal,
      this.tanggalTargetSelesai,
      this.status,
      this.products});

  Orders.fromJson(Map<String, dynamic> json) {
    privilege = json['privilege'];
    id = json['id'];
    idOrder = json['id_order'];
    idCustomer = json['id_customer'];
    namaPasien = json['nama_pasien'];
    customer = json['customer'];
    tanggal = json['tanggal'];
    tanggalTargetSelesai = json['tanggal_target_selesai'];
    status = json['status'];
    if (json['products'] != null) {
      products = new List<Products>();
      json['products'].forEach((v) {
        products.add(new Products.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['privilege'] = this.privilege;
    data['id'] = this.id;
    data['id_order'] = this.idOrder;
    data['id_customer'] = this.idCustomer;
    data['nama_pasien'] = this.namaPasien;
    data['customer'] = this.customer;
    data['tanggal'] = this.tanggal;
    data['tanggal_target_selesai'] = this.tanggalTargetSelesai;
    data['status'] = this.status;
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Products {
  int idProduct;
  String namaProduct;
  int selesai;
  int totalPekerjaan;
  int status;

  Products(
      {this.idProduct, this.status, this.namaProduct, this.selesai, this.totalPekerjaan});

  Products.fromJson(Map<String, dynamic> json) {
    idProduct = json['id_product'];
    namaProduct = json['nama_product'];
    selesai = json['selesai'];
    totalPekerjaan = json['total_pekerjaan'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_product'] = this.idProduct;
    data['nama_product'] = this.namaProduct;
    data['selesai'] = this.selesai;
    data['total_pekerjaan'] = this.totalPekerjaan;
    data['status'] = this.status;
    return data;
  }
}
