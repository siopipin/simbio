import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:simbio/src/produk/screens/pager.dart';
class ProdukScreen extends StatelessWidget {
  const ProdukScreen({Key key}) : super(key: key);
  MenuHomePage(){
    SystemChrome.setPreferredOrientations(
        <DeviceOrientation>[DeviceOrientation.portraitUp]);
  }
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Stack(
        alignment: AlignmentDirectional.topEnd,
        children: <Widget>[
          new MenuPager(),
        ],
      ),
    );
  }
}