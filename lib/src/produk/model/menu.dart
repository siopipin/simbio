import 'package:simbio/src/produk/model/food.dart';

class Menu {
  static List<Food> menu = [
    Food(
        id: "1",
        image: "assets/images/menu1.png",
        name: "Gigi Palsu",
        price: "\$12"),
    Food(
        id: "3",
        image: "assets/images/menu1.png",
        name: "Kerangka Gigi",
        price: "\$4"),
    Food(
        id: "4",
        image: "assets/images/menu1.png",
        name: "Gigi Geraham",
        price: "\$30"),
    Food(
        id: "5",
        image: "assets/images/menu1.png",
        name: "Gigi Besih",
        price: "\$22"
        ),
    Food(
        id: "2",
         image: "assets/images/menu1.png",
        name: "Gigi Besih",
        price: "\$22"),
     Food(
        id: "6",
        image: "assets/images/menu1.png",
        name: "Gigi Palsu",
        price: "\$12"),
    Food(
        id: "7",
        image: "assets/images/menu1.png",
        name: "Kerangka Gigi",
        price: "\$4"),
    Food(
        id: "8",
        image: "assets/images/menu1.png",
        name: "Gigi Geraham",
        price: "\$30"),
    Food(
        id: "9",
        image: "assets/images/menu1.png",
        name: "Gigi Besih",
        price: "\$22"
        ),
    Food(
        id: "20",
         image: "assets/images/menu1.png",
        name: "Gigi Besih",
        price: "\$22"),
  ];

  static Food getFoodById(id) {
    return menu.where((p) => p.id == id).first;
  }
}