import 'package:flutter/material.dart';
import 'package:simbio/src/util/pustaka.dart';

class CartButton extends StatelessWidget {

  CartButton({this.counter, this.addToCart});
  final int counter;
  final VoidCallback addToCart;

  @override
  Widget build(BuildContext context) {
    return new Align(
      alignment: FractionalOffset.bottomCenter,
      child: new SizedBox(
        width: 120.0,
        height: 60.0,
        child: new MaterialButton(
          highlightColor: Colors.grey,
          onPressed: counter == 0 ? null : addToCart,
          elevation: counter == 0 ? 10.0 : 5.0,
          color: counter == 0 ? COLOR.background() : COLOR.secondary(),
          child: Container(
            padding: EdgeInsets.all(4),
            child: Column(
              children: <Widget>[
                new Icon(Icons.attach_money, color: COLOR.ketiga(),),
                new Text('Tambahkan', style: TextStyle(color: COLOR.ketiga()),)
              ],
            ),
          ),
        ),
      ),
    );
  }

}