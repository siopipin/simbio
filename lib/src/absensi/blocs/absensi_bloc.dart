import 'dart:async';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';
import 'package:simbio/src/absensi/recources/absensi_repository.dart';
import 'package:simbio/src/util/response_model.dart';

class AbsensiBloc {
  AbsensiRepository absensiRepository = AbsensiRepository();

  BehaviorSubject<ResponseModel> responseAbsensi =
      BehaviorSubject<ResponseModel>();
  Observable<ResponseModel> get streamPreorder => responseAbsensi.stream;

  doPostAbsensi(
      {@required String idTekniker,
      @required String tanggal,
      @required String tglmasuk}) async {
    ResponseModel responseModel = await absensiRepository.postAbsensi(
        idTekniker: idTekniker, tanggal: tanggal, tglmasuk: tglmasuk);
    responseAbsensi.sink.add(responseModel);
    return responseModel;
  }

  doUpdateAbsensi(
      {@required String idTekniker,
      @required String tanggal,
      @required String tglkeluar}) async {
    ResponseModel responseModel = await absensiRepository.updateAbsensi(
        idTekniker: idTekniker, tanggal: tanggal, tglkeluar: tglkeluar);
    responseAbsensi.sink.add(responseModel);
    return responseModel;
  }

  dis() {
    responseAbsensi.close();
  }
}

final absensiBLOC = AbsensiBloc();
