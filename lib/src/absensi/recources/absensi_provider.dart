import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:meta/meta.dart';
import 'package:http/http.dart' show Client, Response;
import 'package:simbio/src/auth/repository/auth_repository.dart';
import 'package:simbio/src/util/pustaka.dart';
import 'package:simbio/src/util/response_model.dart';

class AbsensiProvider {
  Client client = Client();

  Future<ResponseModel> postAbsensi(
      {@required String idTekniker,
      @required String tanggal,
      @required String tglmasuk}) async {
    Response response;
    final token = await AuthRepository().hasToken();
    final url = '${URL.api()}tekniker/absensi-awal';
    final body = json.encode(
        {"id_pegawai": idTekniker, "tanggal": tanggal, "masuk": tglmasuk});
    print("absensi_provider / postabsensi / body : $body");
    response = await client.post(url,
        headers: {
          HttpHeaders.authorizationHeader: 'Barer $token',
          'Content-type': 'application/json',
          'Accept': 'application/json'
        },
        body: body);
    print(
        "absensi_provider / postabsensi / statuscode : ${response.statusCode}");
    final data = json.decode(response.body);
    data['statuscode'] = response.statusCode;
    if (response.statusCode == 200) {
      data['message'] = "Successfully";
      return ResponseModel.fromJson(data);
    } else {
      data['message'] = "Response Server";
      return ResponseModel.fromJson(data);
    }
  }

  Future<ResponseModel> updateAbsensi(
      {@required String idTekniker,
      @required String tanggal,
      @required String tglkeluar}) async {
    Response response;
    final token = await AuthRepository().hasToken();
    final url = '${URL.api()}tekniker/absensi-akhir/';
    final body = json.encode(
        {"id_pegawai": idTekniker, "tanggal": tanggal, "keluar": tglkeluar});

    print("absensi_provider / updateabsensi / body : $body");

    response = await client.put(url,
        headers: {
          HttpHeaders.authorizationHeader: 'Barer $token',
          'Content-type': 'application/json',
          'Accept': 'application/json'
        },
        body: body);
    print(
        "absensi_provider / updateabsensi / statuscode : ${response.statusCode}");
    final data = json.decode(response.body);
    data['statuscode'] = response.statusCode;
    if (response.statusCode == 200) {
      data['message'] = "Successfully";
      return ResponseModel.fromJson(data);
    } else {
      data['message'] = "Response Server";
      return ResponseModel.fromJson(data);
    }
  }
}
