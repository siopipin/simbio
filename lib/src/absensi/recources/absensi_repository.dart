import 'dart:async';

import 'package:meta/meta.dart';
import 'package:simbio/src/absensi/recources/absensi_provider.dart';
import 'package:simbio/src/util/response_model.dart';

class AbsensiRepository {
  AbsensiProvider absensiProvider = AbsensiProvider();

  Future<ResponseModel> postAbsensi(
          {@required String idTekniker,
          @required String tanggal,
          @required String tglmasuk}) =>
      absensiProvider.postAbsensi(
          idTekniker: idTekniker, tanggal: tanggal, tglmasuk: tglmasuk);

  Future<ResponseModel> updateAbsensi(
          {@required String idTekniker,
          @required String tanggal,
          @required String tglkeluar}) =>
      absensiProvider.updateAbsensi(
          idTekniker: idTekniker, tanggal: tanggal, tglkeluar: tglkeluar);
}
