class AbsensiModel {
  bool status;
  List<Data> data;

  AbsensiModel({this.status, this.data});

  AbsensiModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  int idPegawai;
  String tanggal;
  String masuk;
  String keluar;

  Data({this.id, this.idPegawai, this.tanggal, this.masuk, this.keluar});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idPegawai = json['id_pegawai'];
    tanggal = json['tanggal'];
    masuk = json['masuk'];
    keluar = json['keluar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_pegawai'] = this.idPegawai;
    data['tanggal'] = this.tanggal;
    data['masuk'] = this.masuk;
    data['keluar'] = this.keluar;
    return data;
  }
}